FROM flink

#Install custom software
#RUN set -ex; apt-get update; apt-get -y install python

#ADD /host/path/to/flink-conf.yaml /container/local/path/to/custom/conf/flink-conf.yaml
#ADD /host/path/to/log4j.properties /container/local/path/to/custom/conf/log4j.properties

#Optional library
#RUN ln -fs /opt/flink/opt/flink-queryable-state-runtime-*.jar /opt/flink/lib/.

#Optional Plugin
RUN mkdir -p /opt/flink/plugins/flink-s3-fs-hadoop
RUN ln -fs /opt/flink/opt/flink-s3-fs-hadoop-*.jar /opt/flink/plugins/flink-s3-fs-hadoop/.

#Custom library
#ARG JAR_FILE
#RUN ln -fs target/${JAR_FILE} /opt/flink/lib/.

#ENV VAR_NAME value
#ENV HADOOP_CLASSPATH=$FLINK_HOME/flink-s3-fs-hadoop-*.jar