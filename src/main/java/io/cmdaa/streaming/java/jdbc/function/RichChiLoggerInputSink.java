package io.cmdaa.streaming.java.jdbc.function;

import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class RichChiLoggerInputSink extends RichSinkFunction<Tuple5<String, Long, Long, String, Long>> {

    private static final String UPSERT_CHI_LOG_MESSAGE  =
            "INSERT INTO public.chi_logger_score (msg_id, last_count, last_total_count, score, last_updated) "
                    + "VALUES (?, ?, ?, ?, ?) "
                    + "ON CONFLICT (msg_id) "
                    + "DO UPDATE SET last_count = EXCLUDED.last_count, last_totaL_count = EXCLUDED.last_total_count, score = EXCLUDED.score, last_updated = EXCLUDED.last_updated";

    private PreparedStatement statement;

    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection connection =
                DriverManager.getConnection(
                        "jdbc:postgresql://postgres-postgresql.cmdaa.svc.cluster.local:5432/postgres?user=cmdaa&password=cmdaa");

        statement = connection.prepareStatement(UPSERT_CHI_LOG_MESSAGE);
    }

    @Override
    public void invoke(Tuple5<String, Long, Long, String, Long> message, Context context) throws Exception {

        statement.setString(1, message.f0);
        statement.setLong(2, message.f1);
        statement.setLong(3, message.f2);
        statement.setString(4, message.f3);
        statement.setLong(5, message.f4);
        statement.addBatch();
        statement.executeBatch();
    }

}
