package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.model.LogDataTable;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.java.tuple.Tuple5;


import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.MemorySize;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.streaming.api.functions.sink.filesystem.BucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.SimpleVersionedStringSerializer;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/** Use Flink's state to perform efficient record joining based on business requirements. */
public class GrokFailures {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.enableCheckpointing(60 * 1000);
        env.getCheckpointConfig().setCheckpointTimeout(3 * 60 * 1000);

        // switch to batch mode on demand
        // env.setRuntimeMode(RuntimeExecutionMode.BATCH);

        final ParameterTool params = ParameterTool.fromArgs(args);

        final String kafkaServer;
        final String kafkaInputTopic;
        final String kafkaFailuresTopic;
        final int rolloverInterval;
        final long maxPartSize;


        if(params.has("kafka-server")) {
            kafkaServer = params.get("kafka-server");
        } else {
            kafkaServer = "kafka.cmdaa.svc.cluster.local:9092";
            //kafkaServer = "kafka-cp-kafka-headless:9092";
        }

        if(params.has("kafka-input-topic")) {
            kafkaInputTopic = params.get("kafka-input-topic");
        } else {
            kafkaInputTopic = "fluentd";
        }

        if(params.has("kafka-failures-topic")) {
            kafkaFailuresTopic = params.get("kafka-failures-topic");
        } else {
            kafkaFailuresTopic = "grokfailures";
        }

        if(params.has("rollover-interval")) {
            rolloverInterval = Math.abs(Integer.parseInt(params.get("rollover-interval")));
        } else {
            rolloverInterval = 15;
        }

        if(params.has("max-part-size")) {
            //maxPartSize = MemorySize.parse(params.get("max-part-size"), MEGA_BYTES).getBytes();
            maxPartSize = MemorySize.parseBytes(params.get("max-part-size"));
            //maxPartSize = Math.abs(Long.parseLong(params.get("max-part-size"))) * (long) Math.pow(1024, 2);
        } else {
            maxPartSize = MemorySize.parseBytes("1gb");
        }

        // read transactions
        KafkaSource<FluentdMessage> transactionSource =
                KafkaSource.<FluentdMessage>builder()
                        .setBootstrapServers(kafkaServer)
                        .setTopics(kafkaInputTopic)
                        .setGroupId("")
                        .setDeserializer(new FluentdRecordDeserializer())
                        .setStartingOffsets(OffsetsInitializer.latest())
                        // .setBounded(OffsetsInitializer.latest())
                        .build();

        KafkaSource<LogDataTable> lookupSource =
                KafkaSource.<LogDataTable>builder()
                        .setBootstrapServers(kafkaServer)
                        .setTopics(kafkaFailuresTopic)
                        .setGroupId("")
                        .setDeserializer(new LogDataTableDeserializer())
                        // .setBounded(OffsetsInitializer.latest())
                        .build();

        DataStream<FluentdMessage> transactionStream =
                env.fromSource(transactionSource, WatermarkStrategy.noWatermarks(), "Fluentd")
                        .uid("transaction-id");

        // find messages with no grok name
        DataStream<FluentdMessage> filterStream =
                transactionStream
                        .filter(m -> m.getGrokName() == null)
                        //.keyBy(f -> f.getLogId())
                        .process(new ProcessFunction<FluentdMessage, FluentdMessage>() {

                            @Override
                            public void processElement(
                                    FluentdMessage message,
                                    ProcessFunction<FluentdMessage, FluentdMessage>.Context ctx,
                                    Collector<FluentdMessage> out) throws Exception {
                                out.collect(message);
                            }
                        });

        DataStream<LogDataTable> lookupStream =
                env.fromSource(lookupSource, WatermarkStrategy.noWatermarks(), "Grokfailures")
                        .uid("lookup-id");

        // join fluentd messages and grokfailures
        DataStream<Tuple5<String, String, String, String, Long>> joinedStream = lookupStream
                .connect(filterStream)
                .keyBy(g -> g.getLogName(), f -> f.getLogId())
                .process(
                        new KeyedCoProcessFunction<String, LogDataTable, FluentdMessage, Tuple5<String, String, String, String, Long>>() {

                            ValueState<LogDataTable> logDataTable;

                            ListState<FluentdMessage> messages;

                            @Override
                            public void open(Configuration parameters) {
                                logDataTable =
                                        getRuntimeContext()
                                                .getState(new ValueStateDescriptor<>("data", LogDataTable.class));
                                messages =
                                        getRuntimeContext()
                                                .getListState(new ListStateDescriptor<>("transactions", FluentdMessage.class));
                            }

                            @Override
                            public void processElement1(
                                    LogDataTable c,
                                    KeyedCoProcessFunction<String, LogDataTable, FluentdMessage, Tuple5<String, String, String, String, Long>>.Context ctx,
                                    Collector<Tuple5<String, String, String, String, Long>> out)
                                    throws Exception {
                                logDataTable.update(c);
                                Iterator<FluentdMessage> txs = messages.get().iterator();
                                // buffer transactions if the other stream is not ready yet
                                if (!txs.hasNext()) {
                                    performJoin(out, c, txs);
                                }
                            }

                            @Override
                            public void processElement2(
                                    FluentdMessage tx,
                                    KeyedCoProcessFunction<String, LogDataTable, FluentdMessage, Tuple5<String, String, String, String, Long>>.Context ctx,
                                    Collector<Tuple5<String, String, String, String, Long>> out)
                                    throws Exception {
                                messages.add(tx);
                                LogDataTable c = logDataTable.value();
                                // buffer customer if the other stream is not ready yet
                                if (logDataTable.value() != null) {
                                    performJoin(out, c, messages.get().iterator());
                                }
                            }

                            private void performJoin(
                                    Collector<Tuple5<String, String, String, String, Long>> out, LogDataTable c, Iterator<FluentdMessage> txs) {
                                //txs.forEachRemaining(tx -> out.collect(c.getS3Path() + ": " + tx.getMessage()));
                                txs.forEachRemaining(tx -> out.collect(Tuple5.of(c.getLogId(), c.getS3Path(), tx.getMessage(), tx.getHostname(), c.getInsertTime())));
                                messages.clear();
                            }
                        });

        OutputFileConfig config = OutputFileConfig
                .builder()
                .withPartPrefix("cmdaa")
                .withPartSuffix(".log")
                .build();

        final StreamingFileSink<Tuple5<String, String, String, String, Long>> sink =
                StreamingFileSink.forRowFormat(
                                //new Path("s3a://flink.cmdaa/hadoop_2k"),
                                new Path("s3://cmdaa/"),
                                new CSVEncoder())
                        .withBucketAssigner(new KeyBucketAssigner())
                        .withRollingPolicy(
                                DefaultRollingPolicy.builder()
                                        .withRolloverInterval(TimeUnit.MINUTES.toMillis(rolloverInterval))
                                        .withInactivityInterval(TimeUnit.MINUTES.toMillis(5))
                                        .withMaxPartSize(maxPartSize)
                                        .build())
                        //.withRollingPolicy(OnCheckpointRollingPolicy.build())
                        .withOutputFileConfig(config)
                        .build();

        joinedStream.addSink(sink);

        env.execute("Streaming Grok Failures Join v2");
    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }


        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }

    private static class LogDataTableDeserializer implements KafkaRecordDeserializationSchema<LogDataTable> {

        static ObjectMapper objectMapper;

        static {
//            SimpleModule simpleModule = new SimpleModule("LogDeserializer");
//            simpleModule.addDeserializer(LogDataTable.class, new io.cmdaa.streaming.java.kafka.schema.LogDataTableDeserializer(LogDataTable.class));
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }

        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<LogDataTable> out) throws IOException {
            out.collect(objectMapper.readValue(consumerRecord.value(), LogDataTable.class));
        }

        @Override
        public TypeInformation<LogDataTable> getProducedType() {
            return TypeInformation.of(LogDataTable.class);
        }
    }

    public static class CSVEncoder extends SimpleStringEncoder<Tuple5<String, String, String, String, Long>> {

        public CSVEncoder() {
            super();
        }

        @Override
        public void encode(Tuple5<String, String, String, String, Long> element, OutputStream stream) throws IOException {
            //super.encode(element, stream);
            String trimmed = element.f2;
            stream.write(trimmed.getBytes(StandardCharsets.UTF_8));
            stream.write(10);
        }
    }

    /** Use first field for buckets. */
    public static final class KeyBucketAssigner implements BucketAssigner<Tuple5<String, String, String, String, Long>, String> {

        private static final long serialVersionUID = 987325769970523326L;

        @Override
        public String getBucketId(final Tuple5<String, String, String, String, Long> element, final Context context) {
//            Row result = logLookup
//                    .stream()
//                    .filter(r -> r.getField(0).equals(element.getField(1)))
//                    .findAny()
//                    .orElse(null);
//            return result.getField(1).toString();
            return removeLastCharOptional(element.f1);
        }

        @Override
        public SimpleVersionedSerializer<String> getSerializer() {
            return SimpleVersionedStringSerializer.INSTANCE;
        }
    }

    public static String removeLastCharOptional(String s) {
        return Optional.ofNullable(s)
                .filter(str -> str.length() != 0)
                .map(str -> str.substring(0, str.lastIndexOf('/')))
                .orElse(s);
    }



}


