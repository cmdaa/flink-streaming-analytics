package io.cmdaa.streaming.java.jdbc.function;

import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class RichLogInputSink2 extends RichSinkFunction<Tuple4<String, String, Long, Long>> {

    private static final String UPSERT_FLUENTD_MESSAGE  =
            "INSERT INTO public.fluentd_table_2 (msg_id, host, total_count, last_updated) "
                    + "VALUES (?, ?, ?, ?) "
                    + "ON CONFLICT (msg_id, host) "
                    + "DO UPDATE SET total_count = EXCLUDED.total_count, last_updated = EXCLUDED.last_updated";

    private PreparedStatement statement;

    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection connection =
                DriverManager.getConnection(
                        "jdbc:postgresql://postgres-postgresql.cmdaa.svc.cluster.local:5432/postgres?user=cmdaa&password=cmdaa");

        statement = connection.prepareStatement(UPSERT_FLUENTD_MESSAGE);
    }

    @Override
    public void invoke(Tuple4<String, String, Long, Long> message, SinkFunction.Context context) throws Exception {

        statement.setString(1, message.f0);
        statement.setString(2, message.f1);
        statement.setLong(3, message.f2);
        statement.setLong(4, message.f3);
        statement.addBatch();
        statement.executeBatch();

    }

}