package io.cmdaa.streaming.java.jdbc.function;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;

public class RichLogInputSink3 extends RichSinkFunction<Tuple3<String, String, Long>> {

    private static final String UPSERT_FLUENTD_MESSAGE  =
            "INSERT INTO public.chi_logger_history (msg_id, hostname, old_count, last_updated) "
                    + "VALUES (?, ?, ?, ?) "
                    + "ON CONFLICT (msg_id, hostname) "
                    + "DO UPDATE SET old_count = EXCLUDED.old_count, last_updated = EXCLUDED.last_updated";;

    private PreparedStatement statement;

    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection connection =
                DriverManager.getConnection(
                        "jdbc:postgresql://cmdaa-postgres.cmdaa.svc.cluster.local:5432/postgres?user=cmdaa&password=3qPiU22ufPbaujvUesY76dILgCfvAx9I");

        statement = connection.prepareStatement(UPSERT_FLUENTD_MESSAGE);
    }

    @Override
    public void invoke(Tuple3<String, String, Long> message, SinkFunction.Context context) throws Exception {

        statement.setString(1, message.f0);
        statement.setString(2, message.f1);
        statement.setLong(3, message.f2);
        statement.setObject(4, LocalDateTime.now());
        statement.addBatch();
        statement.executeBatch();

    }


}
