package io.cmdaa.streaming.java.jdbc;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.InputMessageDeserializationSchema;
//import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.api.connector.source.SourceReader;

import java.util.Properties;

public class TestClass {

    public static void main(String[] args) throws Exception {


        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "kafka-cp-kafka-headless:9092");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //env.setRuntimeMode(RuntimeExecutionMode.STREAMING);

        DataStream<FluentdMessage> stream = env
                //.addSource(getConsumer(properties));
        .addSource(new SourceFunction<FluentdMessage>() {
            @Override
            public void run(SourceContext<FluentdMessage> sourceContext) throws Exception {

            }

            @Override
            public void cancel() {

            }
        });


        DataStream<Tuple2<String, Long>> keyedCounts = stream
                .filter(value -> value.getGrokName() != null)
                .map(new MapFunction<FluentdMessage, Tuple2<String, Long>>() {
                    @Override
                    public Tuple2<String, Long> map(FluentdMessage value) throws Exception {
                        return Tuple2.of(value.getGrokName(), 1L);
                    }
                })
                .keyBy(value -> value.f0)
                .reduce((ReduceFunction<Tuple2<String, Long>>) (data1, data2) -> Tuple2.of(data1.f0, data1.f1 + data2.f1));


        keyedCounts.writeAsText("s3://argo-artifacts/output.txt");
        env.execute("Flink Kafka Chi Log Runner");

    }

    private static FlinkKafkaConsumer<FluentdMessage> getConsumer(Properties props) {

        FlinkKafkaConsumer<FluentdMessage> consumer = new FlinkKafkaConsumer<>(
                "chi-logger-input",
                new InputMessageDeserializationSchema(), props);
        //consumer.assignTimestampsAndWatermarks(WatermarkStrategy.forBoundedOutOfOrderness(Duration.ofSeconds(20)));
//        Map<KafkaTopicPartition, Long> map = new HashMap<KafkaTopicPartition, Long>();
//        map.put(new KafkaTopicPartition("fluentd", 0), 20060L);
//        consumer.setStartFromSpecificOffsets(map);
        consumer.setStartFromEarliest();

        return consumer;
    }






}
