package io.cmdaa.streaming.java.jdbc.triggers;

import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.api.windowing.triggers.Trigger;
import org.apache.flink.streaming.api.windowing.triggers.TriggerResult;

public class PurgeElementsTrigger extends Trigger<Object, TimeWindow> {

        private static final long serialVersionUID = 1L;

        private PurgeElementsTrigger() {}

        @Override
        public TriggerResult onElement(Object element, long timestamp, TimeWindow window, TriggerContext ctx) throws Exception {
            ctx.registerProcessingTimeTimer(window.maxTimestamp());
            return TriggerResult.CONTINUE;
        }

        @Override
        public TriggerResult onEventTime(long time, TimeWindow window, TriggerContext ctx) {
            return TriggerResult.CONTINUE;
        }

        @Override
        public TriggerResult onProcessingTime(long time, TimeWindow window, TriggerContext ctx) throws Exception {
             return time == window.maxTimestamp() ?
                    TriggerResult.PURGE :
                    TriggerResult.CONTINUE;
        }

        @Override
        public void clear(TimeWindow window, TriggerContext ctx) throws Exception {
            ctx.deleteProcessingTimeTimer(window.maxTimestamp());
        }

        @Override
        public boolean canMerge() {
            return true;
        }

        @Override
        public void onMerge(TimeWindow window,
                            OnMergeContext ctx) {
            // only register a timer if the watermark is not yet past the end of the merged window
            // this is in line with the logic in onElement(). If the watermark is past the end of
            // the window onElement() will fire and setting a timer here would fire the window twice.
            long windowMaxTimestamp = window.maxTimestamp();
            if (windowMaxTimestamp > ctx.getCurrentWatermark()) {
                ctx.registerProcessingTimeTimer(windowMaxTimestamp);
            }
        }

        @Override
        public String toString() {
            return "PurgeElementsTrigger()";
        }

        /**
         * Creates an event-time trigger that fires once the watermark passes the end of the window.
         *
         * <p>Once the trigger fires all elements are discarded. Elements that arrive late immediately
         * trigger window evaluation with just this one element.
         */
        public static PurgeElementsTrigger create() {
            return new PurgeElementsTrigger();
        }
}
