package io.cmdaa.streaming.java.jdbc;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.DataSet;

import org.apache.flink.api.common.time.Deadline;
import org.apache.flink.api.common.time.Time;

import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.connector.jdbc.JdbcInputFormat;

import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.api.java.typeutils.TupleTypeInfo;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;

import static org.apache.flink.api.java.aggregation.Aggregations.SUM;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import java.text.DecimalFormat;

public class ChiLoggerRunnerStage2 {

    private final static long MAX_TIME_LAG = 30 * 1000L;

    public static void main(String[] args) throws Exception {

        // set up the execution environment
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();


        TypeInformation[] fieldTypes = new TypeInformation[] {
                BasicTypeInfo.STRING_TYPE_INFO,
                BasicTypeInfo.INT_TYPE_INFO,
                BasicTypeInfo.DATE_TYPE_INFO
        };

        RowTypeInfo rowTypeInfo = new RowTypeInfo(fieldTypes);


        JdbcInputFormat inputFormat = JdbcInputFormat.buildJdbcInputFormat()
                .setDrivername("org.postgresql.Driver")
                .setDBUrl("jdbc:postgresql://postgres-postgresql.cmdaa.svc.cluster.local:5432/postgres?user=cmdaa&password=cmdaa")
                .setQuery("select msg_id, count, last_updated from chi_logger_intake  where last_updated > NOW() - Interval '30 minutes'")
                .setRowTypeInfo(rowTypeInfo)
                .setResultSetType(1)
                .finish();

        TupleTypeInfo typeInfo = new TupleTypeInfo(Tuple3.class,
                BasicTypeInfo.STRING_TYPE_INFO,
                BasicTypeInfo.INT_TYPE_INFO,
                BasicTypeInfo.DATE_TYPE_INFO);

        //DataSet<Tuple3<String, Integer, Date>> dataSet = env.createInput(inputFormat, typeInfo);

        DataSource<Row> rows = env.createInput(inputFormat, rowTypeInfo);
        DataSet<Tuple3<String, String, String>> keyed = rows
                .groupBy(0)
                .aggregate(SUM, 1)
                .map(new MapFunction<Row, Tuple3<String, String, String>>() {
                    @Override
                    public Tuple3<String, String, String> map(Row row) throws Exception {
                        return Tuple3.of(String.valueOf(row.getField(0)),
                                String.valueOf(row.getField(1)),
                                String.valueOf(row.getField(2)));
                    }
                });

//        dataSet.groupBy(0).aggregate(Aggregations.MIN, 2);
//
//        DataSet<Tuple3<String, Long, Long>> sortedSet = dataSet
//                .sortPartition(2, Order.ASCENDING);
//
//        DataSet<Long> startTime = sortedSet.min(2).map(new TimeMinimum());
//
//        sortedSet
//                .filter(new FilterFunction<Tuple3<String, Long, Long>>() {
//                    @Override
//                    public boolean filter(Tuple3<String, Long, Long> value) throws Exception {
//                        return value.f2 < MAX_TIME_LAG;
//                    }
//                });
//
//        //Total Corpus Count
//        DataSet<Tuple3<String, Long, Long>> nonKeyedTotal = dataSet.aggregate(SUM, 1);

        //Total msg_id in Corpus Count
//        DataSet<Tuple3<String, Long, Long>> keyed = dataSet
//                .groupBy(0)
//                .aggregate(SUM, 1)
//                .map(new MapFunction<Tuple3<String, Integer, Date>, Tuple3<String, Long, Long>>() {
//                    @Override
//                    public Tuple3<String, Long, Long> map(Tuple3<String, Integer, Date> value) throws Exception {
//                        return Tuple3.of(value.f0, new Long(value.f1), value.f2.getTime());
//                    }
//                });

//        dataSet
//                .sortPartition(2, Order.ASCENDING)
//                .first(1000);

        //keyed.writeAsText("s3://argo-workflow-bucket/output.txt");
        keyed.writeAsCsv("s3://argo-workflow-bucket/output.txt");
        env.execute("ChiLoggerRunnerStage2");
    }



    /**
     * A reduce function that takes a sequence of edges and builds the adjacency list for the vertex where the edges
     * originate. Run as a pre-processing step.
     */
    @ForwardedFields("0")
    public static final class BuildOutgoingEdgeList implements GroupReduceFunction<Tuple2<Long, Long>, Tuple2<Long, Long[]>> {

        private final List<Long> neighbors = new ArrayList<Long>();

        @Override
        public void reduce(Iterable<Tuple2<Long, Long>> values, Collector<Tuple2<Long, Long[]>> out) {
            neighbors.clear();
            Long id = 0L;

            for (Tuple2<Long, Long> n : values) {
                id = n.f0;
                neighbors.add(n.f1);
            }
            out.collect(new Tuple2<Long, Long[]>(id, neighbors.toArray(new Long[neighbors.size()])));
        }
    }


    /**  */
    @ForwardedFields("0->id")
    public static final class ChiLoggerAverager implements MapFunction<Tuple2<String, Long>, Tuple3<String, Long, String>> {

        @Override
        public Tuple3<String, Long, String> map(Tuple2<String, Long> value) {
            return Tuple3.of(value.f0, value.f1, null);
        }
    }

    @ForwardedFields("0->id")
    public static final class TimeMinimum implements MapFunction<Tuple3<String, Long, Long>, Long> {

        @Override
        public Long map(Tuple3<String, Long, Long> value) {
            return value.f2;
        }
    }


    /**
     * Calculates the cumulative probability of the word occurring in the document collection
     *
     * @param countNew   number of times word occurs in the document
     * @param totalNew total number of words in the document
     * @param countOld   number of times word occurs in document collection
     * @param totalOld total number of words in the document collection
     * @return the cumulative probability of the word occurring in the document collection
     */
    private static String getChiSqrLoggerScore(long countNew, long totalNew, long countOld, long totalOld) {

        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(5);
        double dblCountNew = new Long(countNew).doubleValue();
        double dblTotalNew = new Long(totalNew).doubleValue();
        double dblCountOld = new Long(countOld).doubleValue();
        double dblTotalOld = new Long(totalOld).doubleValue();

        ChiSquaredDistribution chi = new ChiSquaredDistribution(1d);
        double p = dblCountNew / dblTotalNew;
        double q = dblCountOld / dblTotalOld;
        if (p < q) return df.format(0d);
        double t = (dblCountNew + dblTotalOld) / (dblTotalNew + dblTotalOld);
        if (t == 0) {
            System.out.println("attempted division by zero.");
            return df.format(0d);
        }
        double v = dblCountNew * Math.log(p / t) + dblCountOld * Math.log(q / t);
        if (t == 1) return df.format(2d * v);
        if (p < 1) v += (dblTotalNew - dblCountNew) * Math.log((1 - p) / (1 - t));
        if (q < 1) v += (dblTotalOld - dblCountOld) * Math.log((1 - q) / (1 - t));
        v += v;

        return df.format(1d - chi.cumulativeProbability(v));
    }



}
