package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cmdaa.streaming.java.jdbc.function.RichChiLoggerInputSink;
import io.cmdaa.streaming.java.jdbc.function.RichLogInputSink;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.InputMessageDeserializationSchema;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.*;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.PurgingTrigger;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;

import io.cmdaa.streaming.java.jdbc.function.RichLogInputSink3;
import org.apache.flink.streaming.connectors.kafka.internals.KafkaTopicPartition;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Arrays;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ChiLoggerRunnerStage1 {

    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setRuntimeMode(RuntimeExecutionMode.BATCH);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        KafkaSource<FluentdMessage> source = KafkaSource
                .<FluentdMessage>builder()
                .setBootstrapServers("kafka-cp-kafka-headless:9092")
                .setTopics(Arrays.asList("fluentd"))
                .setDeserializer(new FluentdRecordDeserializer())
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setBounded(OffsetsInitializer.latest())
                //.setUnbounded(OffsetsInitializer.latest())
                .build();

        DataStream<FluentdMessage> stream = env.fromSource(source, WatermarkStrategy.forMonotonousTimestamps(), "Kafka Source");

        stream
                .filter(value -> value.getGrokName() != null)
                .filter(value -> value.getGrokName() != "")
                .map(new MapFunction<FluentdMessage, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> map(FluentdMessage value) throws Exception {
                        return Tuple2.of(value.getGrokName(), value.getHostname());
                    }
                })
                //.keyBy(value -> value.f0)
                //.sum(2)
                //.reduce((ReduceFunction<Tuple2<String, Long>>) (data1, data2) -> Tuple2.of(data1.f0, data1.f1 + data2.f1))
                //.addSink(new RichLogInputSink3())
                .name("Postgres Sink");

        env.execute("Flink Kafka Chi Log Stage 1 Runner");

    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }


        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }

}
