package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.cmdaa.streaming.java.jdbc.function.RichChiLoggerInputSink;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.connectors.kafka.internals.KafkaTopicPartition;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.GlobalWindows;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.PurgingTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.StreamSupport;

import java.io.IOException;

public class ChiLogRunnerBatch {

    private static Logger logger = LoggerFactory.getLogger(ChiLogRunnerBatch.class);

    public static void main (String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        Map<TopicPartition, Long> map = new HashMap<TopicPartition, Long>();
        map.put(new TopicPartition("fluentd", 0), 450000L);

        env.setRuntimeMode(RuntimeExecutionMode.BATCH);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        KafkaSource<FluentdMessage> source = KafkaSource
                .<FluentdMessage>builder()
                .setBootstrapServers("kafka-cp-kafka-headless:9092")
                .setTopics(Arrays.asList("fluentd"))
                .setDeserializer(new FluentdRecordDeserializer())
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setBounded(OffsetsInitializer.latest())
                //.setUnbounded(OffsetsInitializer.latest())
                .build();

        WatermarkStrategy<FluentdMessage> watermarkStrategy = WatermarkStrategy.<FluentdMessage>forMonotonousTimestamps()
                .withTimestampAssigner(new SerializableTimestampAssigner<FluentdMessage>() {
                    @Override
                    public long extractTimestamp(FluentdMessage message, long l) {
                        return System.currentTimeMillis();
                    }
                });

        DataStream<FluentdMessage> stream = env.fromSource(source, WatermarkStrategy.forMonotonousTimestamps(), "Kafka Source");

        DataStream<Tuple2<String, Long>> keyedCounts  = stream
                .filter(value -> value.getGrokName() != null)
                .map(new MapFunction<FluentdMessage, Tuple2<String, Long>>() {
                    @Override
                    public Tuple2<String, Long> map(FluentdMessage value) throws Exception {
                        return Tuple2.of(value.getGrokName(), 1L);
                    }
                })
//                .assignTimestampsAndWatermarks(new WatermarkStrategy<Tuple2<String, Long>>() {
//                    @Override
//                    public WatermarkGenerator<Tuple2<String, Long>> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
//                        return new WatermarkGenerator<Tuple2<String, Long>>() {
//                            private final long maxTimeLag = 5000; // 5 seconds
//
//                            @Override
//                            public void onEvent(Tuple2<String, Long> event, long eventTimestamp, WatermarkOutput output) {
//                                // don't need to do anything because we work on processing time
//                            }
//
//                            @Override
//                            public void onPeriodicEmit(WatermarkOutput output) {
//                                output.emitWatermark(new Watermark(System.currentTimeMillis()));
//                            }
//                        };
//                    }
//                })
                .keyBy(value -> value.f0)
                .window(TumblingEventTimeWindows.of(Time.milliseconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                //.sum(2);
                .reduce((ReduceFunction<Tuple2<String, Long>>) (data1, data2) -> Tuple2.of(data1.f0, data1.f1 + data2.f1));


        DataStream<Tuple5<String, Long, Long, String, Long>> globalCounts = keyedCounts
                .windowAll(TumblingEventTimeWindows.of(Time.milliseconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                .process(new ProcessAllWindowFunction<Tuple2<String, Long>, Tuple5<String, Long, Long, String, Long>, TimeWindow>() {

                    private ValueState<Long> totalCount;
                    private ValueState<Map> keyMap;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        totalCount = getRuntimeContext().getState(
                                new ValueStateDescriptor<>("count", Long.class));

                         keyMap = getRuntimeContext().getState(
                                 new ValueStateDescriptor<>("map", Map.class));

                    }

                    @Override
                    public void process(Context context,
                                        Iterable<Tuple2<String, Long>> iterable,
                                        Collector<Tuple5<String, Long, Long, String, Long>> out) throws Exception {

                        if(totalCount.value() == null) {
                            totalCount.update(0L);
                            keyMap.update(setInitialMapValues(iterable));
                        }

                        long nextCount = totalCount.value();

                        Iterator<Tuple2<String, Long>> iterator = iterable.iterator();
                        Map<String, Long> map = new HashMap<>();
                        //Map<String, List<Long>> keyTotalMap = keyMap.value();
                        Map<String, Long> keyTotalMap = keyMap.value();

                        while (iterator.hasNext()) {
                            Tuple2<String, Long> tuple = iterator.next();
                            nextCount += tuple.f1;
                            map.put(tuple.f0, keyDifference(tuple.f0, iterable));

                            //keyTotalMap.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
                            //out.collect(Tuple3.of(tuple.f0, keyDifference(tuple.f0, iterable), sum(iterable)));
                        }

                        long finalNextCount = nextCount;
                        map.forEach((key, value) -> {
                            if(value > 0L) {
                                long keyTotal;
                                if(keyTotalMap.get(key) != null) {
                                     keyTotal = keyTotalMap.get(key) + value;
                                } else {
                                    keyTotal = value;
                                }
                                out.collect(Tuple5.of(
                                        key,
                                        value,
                                        sumKeys(map),
                                        getChiSqrLoggerScore(value, sumKeys(map), keyTotal, finalNextCount),
                                        0L)
                                );
                                keyTotalMap.put(key, keyTotal);
                            }});
                        totalCount.update(finalNextCount);
                        keyMap.update(keyTotalMap);
                        out.collect(Tuple5.of(null, null, null, null, null));
                    }
                })
                //.addSink(new RichChiLoggerInputSink())
                .name("Postgres Sink");

        globalCounts.writeAsText("s3://argo-artifacts/output.txt");

        env.execute("Flink Kafka Chi Log Runner");

    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }


        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }

    private static Map<String, Long> setInitialMapValues(Iterable<Tuple2<String, Long>> values) {
        Map<String, Long> map = new HashMap<>();
        values.forEach(value -> map.put(value.f0, value.f1));
        return map;
    }

    private static void sessionMap(Map<String, Long> map, String key, long count){
        map.put(key, count);
    }

    private static long getTotalCount(Map<String, Long> map) {
        long sum = 0L;
        for(Map.Entry<String, Long> entry : map.entrySet()) {
            sum += entry.getValue();
        }
        return sum;
    }

    private static long sumKeys(Map<String, Long> map) {
        return map.entrySet().stream().mapToLong(l -> l.getValue()).sum();
    }

    private static long sum(Iterable<Tuple2<String, Long>> values) {
        long sum = 0L;
        for (Tuple2<String, Long> value : values) {
            sum += value.f1;
        }
        return sum;
    }

    private static long sum(String key, Iterable<Tuple2<String, Long>> values) {
        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : values) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }
        return map.get(key).stream().mapToLong(l -> l).sum();
    }

    private static long sum(String key, Map<String, List<Long>> map) {

        List<Long> values = map.get(key);

        Collections.sort(values);

        int size = values.size();

        return values.get(size - 1);
    }

    private static long sum(Map<String, List<Long>> map) {
        return map.entrySet().stream().mapToLong(entry -> entry.getValue().get(entry.getValue().size() - 1)).sum();
    }

    private static long keyDifference(String key, Iterable<Tuple2<String, Long>> tuples) {
        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : tuples) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }

        List<Long> values = map.get(key);

        Collections.sort(values);

        int size = values.size();
        if(size >= 2) {
            return values.get(size - 1) - values.get(size - 2);
        } else {
            return values.get(size - 1);
        }
    }

    /**
     * Calculates the cumulative probability of the word occurring in the document collection
     *
     * @param countNew   number of times word occurs in the document
     * @param totalNew total number of words in the document
     * @param countOld   number of times word occurs in document collection
     * @param totalOld total number of words in the document collection
     * @return the cumulative probability of the word occurring in the document collection
     */
    private static String getChiSqrLoggerScore(long countNew, long totalNew, long countOld, long totalOld) {

        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(5);
        double dblCountNew = new Long(countNew).doubleValue();
        double dblTotalNew = new Long(totalNew).doubleValue();
        double dblCountOld = new Long(countOld).doubleValue();
        double dblTotalOld = new Long(totalOld).doubleValue();

        ChiSquaredDistribution chi = new ChiSquaredDistribution(1d);
        double p = dblCountNew / dblTotalNew;
        double q = dblCountOld / dblTotalOld;
        if (p < q) return df.format(0d);
        double t = (dblCountNew + dblTotalOld) / (dblTotalNew + dblTotalOld);
        if (t == 0) {
            System.out.println("attempted division by zero.");
            return df.format(0d);
        }
        double v = dblCountNew * Math.log(p / t) + dblCountOld * Math.log(q / t);
        if (t == 1) return df.format(2d * v);
        if (p < 1) v += (dblTotalNew - dblCountNew) * Math.log((1 - p) / (1 - t));
        if (q < 1) v += (dblTotalOld - dblCountOld) * Math.log((1 - q) / (1 - t));
        v += v;

        return df.format(1d - chi.cumulativeProbability(v));
    }



}
