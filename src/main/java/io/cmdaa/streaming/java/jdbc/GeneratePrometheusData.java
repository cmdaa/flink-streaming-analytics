package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.OutputMessageSerializationSchema;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.UUID;
import java.util.Random;
import java.time.format.DateTimeFormatter;

public class GeneratePrometheusData {


    public static void  main(String[] args) throws Exception {

        Properties kafkaProps = new Properties();
        kafkaProps.setProperty("bootstrap.servers", "kafka-cp-kafka-headless:9092");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        FlinkKafkaProducer<Tuple2<String, Double>> kafkaSink = new FlinkKafkaProducer<>(
                "prometheus-output",
                new OutputMessageSerializationSchema("prometheus-output"),
                kafkaProps,
                FlinkKafkaProducer.Semantic.NONE,
                5);

        DataStream<FluentdMessage> generatedSource = env.addSource(UUIDSource.create(100));

        generatedSource
                .filter(value -> value.getGrokName() != null)
                //.filter(value -> value.getHostname() != "")
                .map(new MapFunction<FluentdMessage, Tuple2<String, Double>>() {
                    @Override
                    public Tuple2<String, Double> map(FluentdMessage value) throws Exception {
                        return Tuple2.of(value.getGrokName(), value.getCount());
                    }
                })
//                .keyBy(new KeySelector<Tuple3<String, String, Long>, Tuple2<String, String>>() {
//                    @Override
//                    public Tuple2<String, String> getKey(Tuple3<String, String, Long> value) throws Exception {
//                        return Tuple2.of(value.f0, value.f1);
//                    }
//                })
                .addSink(kafkaSink);

        env.execute("Generate Prometheus Data");
    }


    private static class UUIDSource implements SourceFunction<FluentdMessage> {

        private static final long serialVersionUID = 1L;
        private Integer[] uuids;

        private UUID uuid;

        private volatile boolean isRunning = true;

        private UUIDSource(int number) {
            uuids = new Integer[number];
        }

        public static UUIDSource create(int number) {
            return new UUIDSource(number);
        }

        @Override
        public void run(SourceContext<FluentdMessage> ctx) throws Exception {

            while (isRunning) {
                for (int i = 0; i < uuids.length; i++) {
                    uuid = UUID.randomUUID();

                    FluentdMessage record = new FluentdMessage();
                    record.setGrokName(String.valueOf(uuid));
                    record.setCount(getRandomNumberUsingNextDouble(100));

                    ctx.collect(record);
                    Thread.sleep(60 * 1000);
                }
            }
        }

        @Override
        public void cancel() {
            isRunning = false;
        }

        private static double getRandomNumberUsingNextDouble(int min) {
            Random random = new Random();
            return random.nextDouble() + min;
        }
    }

    public static class OutputMessageSerializationSchema implements KafkaSerializationSchema<Tuple2<String, Double>> {

        private static final Logger LOG = LoggerFactory.getLogger(OutputMessageSerializationSchema.class);

        static ObjectMapper objectMapper;

        static {

            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }

        private String topic;

        public OutputMessageSerializationSchema(String topic) {
            this.topic = topic;
        }

        @Override
        public ProducerRecord<byte[], byte[]> serialize(Tuple2<String, Double> tuple, @Nullable Long aLong) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            byte[] blob = null;
            byte[] msgId = null;

            ObjectNode node = objectMapper.createObjectNode();

            //node.put("msg_id", tuple.f0);
            //node.put("hostname", tuple.f1);
            node.put("msg_id", tuple.f0);
            node.put("timestamp", LocalDateTime.now().format(formatter));
            node.put("count", tuple.f1);


            try {
                blob = objectMapper.writeValueAsString(node).getBytes();
                msgId = objectMapper.writeValueAsString(tuple.f0).getBytes();
            } catch (JsonProcessingException e) {
                LOG.error("Method threw an error: {}", e.getStackTrace());
            }

            ProducerRecord<byte[], byte[]> record = new ProducerRecord<>(topic, msgId, blob);

            Headers headers = record.headers();
            headers.add("msg_id", msgId);
            headers.add("payload", blob);
            return record;
        }

    }




}
