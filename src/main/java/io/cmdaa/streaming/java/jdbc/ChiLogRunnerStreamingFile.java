package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.functions.sink.filesystem.BucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSinkHelper;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.SimpleVersionedStringSerializer;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.Table;
import org.apache.flink.types.Row;
import org.apache.flink.types.RowKind;
import org.apache.flink.util.CloseableIterator;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ChiLogRunnerStreamingFile {

    private static final Logger LOG = LoggerFactory.getLogger(ChiLogRunnerStreamingFile.class);

    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(60 * 1000);

//        Map<TopicPartition, Long> map = new HashMap<TopicPartition, Long>();
//        map.put(new TopicPartition("fluentd", 0), 450000L);
//
        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.getCheckpointConfig().setCheckpointTimeout(60000);

        KafkaSource<FluentdMessage> source = KafkaSource
                .<FluentdMessage>builder()
                .setBootstrapServers("kafka-cp-kafka-headless:9092")
                .setTopics(Arrays.asList("fluentd"))
                .setGroupId("")
                .setDeserializer(new FluentdRecordDeserializer())
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //.setBounded(OffsetsInitializer.latest())
                .setUnbounded(OffsetsInitializer.latest())
                .build();

        //DataStream<FluentdMessage> stream = env.fromSource(source, WatermarkStrategy.forMonotonousTimestamps(), "Kafka Source");
        DataStream<FluentdMessage> generatedSource = env.addSource(UUIDSource.create(100000));

        OutputFileConfig config = OutputFileConfig
                .builder()
                .withPartPrefix("cmdaa")
                .withPartSuffix(".csv")
                .build();

        final StreamingFileSink<Tuple2<String, Long>> sink =
                StreamingFileSink.forRowFormat(
                        //new Path("s3a://flink.cmdaa/hadoop_2k"),
                        new Path("s3://argo-artifacts/files"),
                        new CSVEncoder())
                        //.withBucketAssigner(new KeyBucketAssigner())
//                        .withRollingPolicy(
//                                DefaultRollingPolicy.builder()
//                                .withRolloverInterval(TimeUnit.MINUTES.toMillis(2))
//                                .withInactivityInterval(TimeUnit.MINUTES.toMillis(5))
//                                .withMaxPartSize(1024 * 1024 * 1024)
//                                .build())
                        .withRollingPolicy(OnCheckpointRollingPolicy.build())
                        .withOutputFileConfig(config)
                        .build();

        AlertSink alertSink = new AlertSink("http://cmdaa-workflows-eventsource-svc.cmdaa.svc.cluster.local:12000/notifications", "bar!");

        generatedSource
                .filter(value -> value.getGrokName() != null)
                .filter(value -> value.getHostname() == null)
                .map(new MapFunction<FluentdMessage, Tuple2<String, Long>>() {
                    @Override
                    public Tuple2<String, Long> map(FluentdMessage value) throws Exception {
                        return Tuple2.of(value.getGrokName(), 1L);
                    }
                })
                .keyBy(value -> value.f0)
                .sum(1)
                //.addSink(sink)
                .addSink(alertSink)
                .setParallelism(1);

        //resultStream.writeAsText("s3://argo-artifacts/output.txt");

        env.execute("Streaming Log Count");
    }

    public static class CSVEncoder extends SimpleStringEncoder<Tuple2<String, Long>> {

        public CSVEncoder() {
            super();
        }

        @Override
        public void encode(Tuple2<String, Long> element, OutputStream stream) throws IOException {
            //super.encode(element, stream);
            String trimmed = element.f0 + "," + element.f1;
            stream.write(trimmed.getBytes(StandardCharsets.UTF_8));
            stream.write(10);
        }

    }

    /** Use first field for buckets. */
    public static final class KeyBucketAssigner implements BucketAssigner<Tuple2<String, Long>, String> {

        private static final long serialVersionUID = 987325769970523326L;

        @Override
        public String getBucketId(final Tuple2<String, Long> element, final Context context) {
            return element.getField(0);
        }

        @Override
        public SimpleVersionedSerializer<String> getSerializer() {
            return SimpleVersionedStringSerializer.INSTANCE;
        }
    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }

        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }

    /** Creates an empty temporary directory for CSV files and returns the absolute path. */
    private static String createTemporaryDirectory() throws IOException {
        final java.nio.file.Path tempDirectory = Files.createTempDirectory("cmdaa");
        return tempDirectory.toString();
    }

    private static class AlertSink extends RichSinkFunction<Tuple2<String, Long>> {

        private String message;
        private String webhook;
        private HttpURLConnection con;

        public AlertSink(String webhook, String message) {
            this.webhook = webhook;
            this.message = message;
        }

        @Override
        public void open(Configuration config) throws Exception {
            URL url = new URL(webhook);
            con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-Type", "application/json");

            con.setDoOutput(true);
        }

        @Override
        public void invoke(Tuple2<String, Long> value, Context context) throws Exception {

            String jsonInputString = "{\"subject\": \"chi-logger-alert\", \"message\": \"" + message + "\"" + "}";

            byte[] out = jsonInputString.getBytes(StandardCharsets.UTF_8);

            LOG.info("GETTING HERE!");

            try {
                OutputStream stream = con.getOutputStream();
                stream.write(out);
                System.out.println(con.getResponseCode() + " " + con.getResponseMessage());
            } catch (IOException e) {
                LOG.info(e.getMessage(), e);
            } finally {
                con.disconnect();
            }

        }
    }



    private static class UUIDSource implements SourceFunction<FluentdMessage> {

        private static final long serialVersionUID = 1L;
        private Integer[] uuids;

        private UUID uuid;

        private volatile boolean isRunning = true;

        private UUIDSource(int number) {
            uuids = new Integer[number];
        }

        public static UUIDSource create(int number) {
            return new UUIDSource(number);
        }

        public void run(SourceContext<FluentdMessage> ctx) throws Exception {

            while (isRunning) {
//                Thread.sleep(20 * 1000);
//                FluentdMessage message1 = new FluentdMessage();
//                message1.setGrokName("d56658a7-7566-40c6-822a-d3ac60feab1a");
//                //message1.setHostname("dannunzio");
//                FluentdMessage message2 = new FluentdMessage();
//                message2.setGrokName("606bb0a6-5a64-3eac-9635-6424dfccdc68");
//                //message2.setHostname("dannunzio");
//                FluentdMessage message3 = new FluentdMessage();
//                message3.setGrokName("ad7a6946-2b65-490d-a510-a6e2568f3c0e");
//                //message3.setHostname("dannunzio");
//
//                ctx.collectWithTimestamp(message1, System.currentTimeMillis());
//                ctx.collectWithTimestamp(message2, System.currentTimeMillis());
//                ctx.collectWithTimestamp(message3, System.currentTimeMillis());

                for (int i = 0; i < uuids.length; i++) {
                    uuid = UUID.randomUUID();
                    Thread.sleep(20 * 1000);
                    FluentdMessage record = new FluentdMessage();
                    record.setGrokName(uuid.toString());
                    ctx.collectWithTimestamp(record, System.currentTimeMillis());
                    //ctx.collectWithTimestamp(record, System.currentTimeMillis());
                    ctx.emitWatermark(new Watermark(System.currentTimeMillis()));
                }
            }
        }

        @Override
        public void cancel() {
            isRunning = false;
        }
    }





}
