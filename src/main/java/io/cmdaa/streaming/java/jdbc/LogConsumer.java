package io.cmdaa.streaming.java.jdbc;

import io.cmdaa.streaming.java.jdbc.function.RichLogInputSink;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.InputMessageDeserializationSchema;
import io.cmdaa.streaming.java.kafka.function.GrokFilter;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;


import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple;

import org.apache.flink.streaming.connectors.kafka.internals.KafkaTopicPartition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Properties;
import java.util.Map;

public class LogConsumer {

    private static Logger logger = LoggerFactory.getLogger(LogConsumer.class);

    private static final ZoneId EST_ZONE = ZoneId.of("America/New_York");

    public static void main(String[] args) throws Exception {
        // set up the batch execution environment
        System.out.println("STARTING JOB");

        final ZonedDateTime zonedDateTime = ZonedDateTime.now(EST_ZONE);

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "kafka-cp-kafka-headless:9092");
//        properties.setProperty("group.id", "flink-group");
//        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "io.confluent.kafka.serializers.json.KafkaJsonSchemaDeserializer");
//        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializerr");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

//        FlinkJedisPoolConfig conf = new FlinkJedisPoolConfig.Builder()
//                .setHost("127.0.0.1")
//                .build();

        DataStream<FluentdMessage> stream = env
                .addSource(getConsumer(properties))
                .name("Kafka Source");

/*        stream
                //.filter(new FilterData())
                //.map(new RedisValue())
                .keyBy(value -> value.getGrokName())
                .reduce(new ReduceByGrokName())
                .addSink(new RichLogInputSink())
                //.addSink(new RedisSink<Tuple2<String, String>>(conf, new RedisExampleMapper()));
                .name("Postgres Sink");*/

//        DataStream<Tuple4<String, String, Integer, Integer>> counts =
//                stream.map(new MapByGrokName())
//                        .keyBy(value -> value.f0).sum(3);
//        DataStream<Tuple4<String, String, Integer, Integer>> counts = stream
//                .map(new MapByGrokName())
//                .keyBy(new GrokKeySelector())
//                .timeWindow(Time.milliseconds(1000))
//                .sum(3);
//        DataStream<Tuple5<String, String, Integer, Long, Long>> counts = stream
//                .keyBy(new GrokKeySelector())
//                .process(new CountWithTimeoutFunction());
                //.addSink(new RichLogInputSink());

//        stream
//                .filter(new GrokFilter())
//                .keyBy(
//                        new KeySelector<FluentdMessage, Tuple3<String, String, Integer>>() {
//                            @Override
//                            public Tuple3<String, String, Integer> getKey(FluentdMessage value) throws Exception {
//                                return Tuple3.of(value.getGrokName(), value.getHostname(), value.getHourOfDay());
//                            }
//                        }
//                )
//                .process(new CountWithTimeoutFunction())
//                .addSink(new RichLogInputSink());

        stream
                .filter(new GrokFilter())
                .map(new MapFunction<FluentdMessage, Tuple5<String, String, Integer, Long , Long>>() {
                    @Override
                    public Tuple5<String, String, Integer, Long, Long> map(FluentdMessage value) throws Exception {
                        return Tuple5.of(value.getGrokName(),  value.getHostname(),
                                zonedDateTime.getHour(), 1L, System.currentTimeMillis());
                    }
                })
                .keyBy(new KeySelector<Tuple5<String, String, Integer, Long, Long>, Tuple3<String, String, Integer>>() {
                    @Override
                    public Tuple3<String, String, Integer> getKey(Tuple5<String, String, Integer, Long, Long> value) throws Exception {
                        return Tuple3.of(value.f0, value.f1, value.f2);
                    }
                }).sum(3)
                .addSink(new RichLogInputSink())
                .name("Postgres Sink");



        //counts.writeAsText("s3://argo-workflow-bucket/output.txt");

        //counts.writeAsText("s3://argo-workflow-bucket/output.txt");
        //counts.print();

        // execute program
        env.execute("Flink Kafka Log Consumer");
   }


    private static FlinkKafkaConsumer<FluentdMessage> getConsumer(Properties props) {

        FlinkKafkaConsumer<FluentdMessage> consumer = new FlinkKafkaConsumer<FluentdMessage>(
                "fluentd",
                new InputMessageDeserializationSchema(), props);
        Map<KafkaTopicPartition, Long> map = new HashMap<KafkaTopicPartition, Long>();
        map.put(new KafkaTopicPartition("fluentd", 0), 1L);
        consumer.setStartFromSpecificOffsets(map);
        //consumer.setStartFromLatest();

        return consumer;
    }

    private static class FieldKeyExtractor<T extends Tuple, K> implements KeySelector<T, K> {

        @Override
        @SuppressWarnings("unchecked")
        public K getKey(T value) {
            return (K) value.getField(0);
        }
    }


}
