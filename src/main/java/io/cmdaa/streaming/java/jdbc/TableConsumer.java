package io.cmdaa.streaming.java.jdbc;

import org.apache.flink.api.common.eventtime.WatermarkGenerator;
import org.apache.flink.api.common.eventtime.WatermarkGeneratorSupplier;
import org.apache.flink.api.common.eventtime.WatermarkOutput;
import org.apache.flink.api.common.eventtime.Watermark;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.Encoder;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.connector.jdbc.catalog.PostgresCatalog;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.io.SimpleVersionedSerializer;

import org.apache.flink.streaming.api.datastream.IterativeStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.sink.filesystem.BucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.SimpleVersionedStringSerializer;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;

import org.apache.flink.configuration.GlobalConfiguration;

import org.apache.flink.runtime.util.HadoopUtils;
import java.io.PrintStream;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.PurgingTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.table.api.Table;
import org.apache.flink.types.RowKind;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

import org.apache.flink.table.expressions.OverCall;
import org.apache.flink.table.api.GroupWindowedTable;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.api.bridge.java.BatchTableEnvironment;
import org.apache.flink.streaming.api.functions.sink.PrintSinkFunction;

import org.apache.flink.table.api.Slide;
import org.apache.flink.table.api.Over;
import org.apache.flink.table.api.Tumble;
import org.apache.flink.table.api.ImplicitExpressionConversions;

import org.apache.flink.connector.jdbc.catalog.JdbcCatalog;
import org.apache.flink.connector.jdbc.catalog.JdbcCatalogUtils;
import org.apache.flink.table.catalog.Catalog;

import org.apache.flink.util.CloseableIterator;

import org.apache.flink.table.functions.ScalarFunction;

import org.apache.flink.api.java.ExecutionEnvironment;

import org.apache.flink.table.expressions.Expression;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

import java.util.Iterator;

import java.util.List;
import java.util.ArrayList;

import static org.apache.flink.table.api.Expressions.*;

public class TableConsumer {

    private static TypeInformation<Tuple3<String, String, Long>> typeInfo
            = TypeInformation.of(new TypeHint<Tuple3<String, String, Long>>(){});

    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(100L);
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

//        final EnvironmentSettings settings =
//                EnvironmentSettings.newInstance().inStreamingMode().build();
//        final TableEnvironment tEnv = TableEnvironment.create(settings);

        //BatchTableEnvironment tEnv = BatchTableEnvironment.create(env);

//        String name =  "cmdaa";
//        String defaultDatabase = "postgres";
//        String username = "cmdaa";
//        String password = "cmdaa";
//        String baseUrl = "jdbc:postgresql://postgres-postgresql.cmdaa.svc.cluster.local:5432";
//
//        Catalog catalog = JdbcCatalogUtils.createCatalog(name, defaultDatabase, username, password, baseUrl);
//        tEnv.registerCatalog("cmdaa", catalog);
//
//        // set the JdbcCatalog as the current catalog of the session
//        tEnv.useCatalog("cmdaa");


        String ddl = "CREATE TABLE log_counts (\n" +
                "  msg_id STRING,\n" +
                "  hostname STRING,\n" +
                //"  msg_count BIGINT,\n" +
                "  last_updated TIMESTAMP(3),\n" +
                "  WATERMARK FOR last_updated AS last_updated - INTERVAL '5' SECOND\n" +
                ") WITH (\n" +
                "  'connector.type' = 'jdbc',\n" +
                "  'connector.url' = 'jdbc:postgresql://cmdaa-postgres.cmdaa.svc.cluster.local:5432/postgres',\n" +
                "  'connector.table' = 'chi_logger_intake',\n" +
                "  'connector.driver' = 'org.postgresql.Driver',\n" +
                "  'connector.username' = 'cmdaa',\n" +
                "  'connector.password' = '3qPiU22ufPbaujvUesY76dILgCfvAx9I'\n" +
                ")";
        tEnv.executeSql(ddl);

//        Table totalCount = tEnv.from("log_counts")
//                .select($("msg_id").count());

//        tEnv
//                .sqlQuery(
//                        "SELECT " +
//                                "  COUNT(*) AS `total count` " +
//                                "FROM `log_counts`"
//                )
//                .execute()
//                .print();

//        Table table = tEnv.from("log_counts")
//                //.where($("last_updated").isGreater("NOW() - Interval '30 minutes'"))
//                .where($("hostname").isNotNull()
//                        .and($("hostname").isNotEqual("")))
//                .window(Slide.over(lit(30).minutes())
//                        .every(lit(30).seconds())
//                        .on($("last_updated"))
//                        .as("w"))
//                .groupBy($("msg_id"), $("hostname"), $("w"))
//                .select($("msg_id"),
//                        $("hostname"),
//                        $("msg_id").count().over($("w")));
//                        //$("msg_id").max().over($("w")));
//                        //$("last_updated").max());

        Table log_counts = tEnv.from("log_counts")
                .filter($("hostname").isNotNull()
                        .and($("hostname").isNotEqual("")))
                .window(Tumble
                        .over(lit(30).seconds())
                        .on($("last_updated")).as("w"))
                .groupBy($("msg_id"), $("hostname"), $("w"))
                .select($("msg_id"),
                        $("hostname"),
                        $("msg_id").count().as("cnt"));
//
//        Table total_count = tEnv.from("log_counts")
//                .filter($("hostname").isNotNull()
//                        .and($("hostname").isNotEqual("")))
//                .window(Tumble
//                        .over(lit(1).minutes())
//                        .on($("last_updated")).as("w"))
//                .groupBy($("w"))
//                .select($("msg_id").count().as("cnt")).distinct();


//        Table result = tEnv.from("log_counts")
//                .filter($("hostname").isNotNull()
//                        .and($("hostname").isNotEqual("")))
//                // define window
//                .window(
//                        Over
//                                .partitionBy($("msg_id"), $("hostname"))
//                                .orderBy($("last_updated"))
//                                .preceding(lit(5).minutes())
//                                //.preceding(UNBOUNDED_RANGE)
//                                //.following(CURRENT_RANGE)
//                                .as("w"))
//                // sliding aggregate
//                .select(
//                        $("msg_id"),
//                        $("hostname"),
//                        $("msg_id").count().over($("w"))
//                );

//        Table newTable = tEnv.from("log_counts")
//                .where($("hostname").isNotNull()
//                        .and($("hostname").isNotEqual("")))
//                .groupBy($("msg_id"), $("hostname"))
//                .select($("msg_id"),
//                        $("hostname"),
//                        $("msg_id").count().as("cnt"));

        //tEnv.from("log_counts");

//        Table table = tEnv.sqlQuery(
//                "SELECT msg_id, COUNT(msg_id) as cnt FROM log_counts where last_updated > NOW() - Interval '30 minutes' GROUP BY msg_id, hostname");

       //Table count = tEnv.sqlQuery("SELECT COUNT(msg_id) as cnt FROM log_counts where msg_id = 'a25a9105-fb9e-314d-b428-ac650fb36fe7'");


        //tEnv.toAppendStream(table, Row.class).print();

        try (CloseableIterator<Row> iterator = log_counts.execute().collect()) {
            final List<Row> materializedUpdates = new ArrayList<>();
            iterator.forEachRemaining(
                    row -> {
                        final RowKind kind = row.getKind();
                        switch (kind) {
                            case INSERT:
                            case UPDATE_AFTER:
                                row.setKind(RowKind.INSERT); // for full equality
                                materializedUpdates.add(row);
                                break;
                            case UPDATE_BEFORE:
                            case DELETE:
                                row.setKind(RowKind.INSERT); // for full equality
                                materializedUpdates.remove(row);
                                break;
                        }
                    });
            // show the final output table if the result is bounded,
            // the output should exclude San Antonio because it has a smaller population than
            // Houston or Dallas in Texas (TX)
            materializedUpdates.forEach(System.out::println);
        }

        //DataStream<Tuple2<Boolean, Row>> stream = tEnv.toRetractStream(log_counts, Row.class);
        //DataStream<Row> resultStream = tEnv.toAppendStream(log_counts, Types.ROW(Types.STRING, Types.STRING, Types.LONG));

//        final StreamingFileSink<Row> sink =
//                StreamingFileSink.forRowFormat(
//                        new Path("s3://argo-artifacts/"),
//                        (Encoder<Row>)
//                                (element, stream) -> {
//                                    PrintStream out = new PrintStream(stream);
//                                    out.println(element.toString());
//                                })
//                        .withBucketAssigner(new KeyBucketAssigner())
//                        .withRollingPolicy(OnCheckpointRollingPolicy.build())
//                        .build();
//
//        resultStream
//                .addSink(sink)
//                .setParallelism(1);
        //resultStream.writeAsText("s3://argo-artifacts/output.txt");

/*        DataStream<Tuple3<String, String, Long>> keyedStream = stream
                .filter(new FilterFunction<Tuple2<Boolean, Row>>() {
                    @Override
                    public boolean filter(Tuple2<Boolean, Row> row) throws Exception {
                        return row.getField(0);
                    }
                })
                .assignTimestampsAndWatermarks(new WatermarkStrategy<Tuple2<Boolean, Row>>() {
                    @Override
                    public WatermarkGenerator<Tuple2<Boolean, Row>> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
                       return new WatermarkGenerator<Tuple2<Boolean, Row>>() {
                           @Override
                           public void onEvent(Tuple2<Boolean, Row> row, long l, WatermarkOutput out) {
                               out.emitWatermark(new Watermark(System.currentTimeMillis()));
                           }

                           @Override
                           public void onPeriodicEmit(WatermarkOutput out) {

                           }
                       };
                    }
                })
                .map(new MapFunction<Tuple2<Boolean, Row>, Tuple3<String, String, Long>>() {
                    @Override
                    public Tuple3<String, String, Long> map(Tuple2<Boolean, Row> value) throws Exception {
                        String key = String.valueOf(value.f1.getField(0));
                        String host = String.valueOf(value.f1.getField(1));
                        Long count = Long.valueOf(String.valueOf(value.f1.getField(2)));
                        return Tuple3.of(key, host, count);
                    }
                })
                .windowAll(SlidingEventTimeWindows.of(Time.minutes(30), Time.seconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                .process(new ProcessAllWindowFunction<Tuple3<String, String, Long>, Tuple3<String, String, Long>, TimeWindow>() {

                    @Override
                    public void process(Context context, Iterable<Tuple3<String, String, Long>> iterable, Collector<Tuple3<String, String, Long>> out) throws Exception {
                            Iterator<Tuple3<String, String, Long>> iterator = iterable.iterator();
                            while(iterator.hasNext()) {
                                out.collect(iterator.next());
                            }
                            out.collect(Tuple3.of(null, null, null));
                         }
                     }
                );*/


//        Table result = tEnv.sqlQuery(
//                "SELECT\n" +
//                        " msg_id,\n" +
//                        " host,\n" +
//                        " hour_of_day,\n" +
//                        " COUNT(*) cnt,\n" +
//                        " max(last_updated) last_updated\n" +
//                        "FROM log_counts\n" +
//                        "GROUP BY msg_id, host, hour_of_day"
//        );

                        //DataStream<Tuple2<Boolean, TableMessage>> stream = tEnv.toRetractStream(result, TableMessage.class);
                        //tEnv.toAppendStream(result, TableMessage.class).print();

                        //tEnv.toDataSet(result, TableMessage.class).print();


                        env.execute("Streaming Log Count");

    }

    public static class InputMap implements MapFunction<Row, Tuple3<String, String, Long>> {
        private static final long serialVersionUID = 1L;

        @Override
        public Tuple3<String, String, Long> map(Row value) throws Exception {
            return Tuple3.of(String.valueOf(value.getField(0)), String.valueOf(value.getField(1)), Long.valueOf(String.valueOf(value.getField(2))));
        }
    }


    /** Use first field for buckets. */
    public static final class KeyBucketAssigner implements BucketAssigner<Row, String> {

        private static final long serialVersionUID = 987325769970523326L;

        @Override
        public String getBucketId(final Row element, final Context context) {
            return String.valueOf(element.getField(0));
        }

        @Override
        public SimpleVersionedSerializer<String> getSerializer() {
            return SimpleVersionedStringSerializer.INSTANCE;
        }
    }

    private static class AggregationFunction extends ScalarFunction {

        public long eval(Long value) {
            return Long.MIN_VALUE;
        }


    }
}
