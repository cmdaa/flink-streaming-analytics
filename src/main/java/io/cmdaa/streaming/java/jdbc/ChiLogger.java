package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.PurgingTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import io.cmdaa.streaming.java.kafka.schema.OutputMessageSerializationSchema;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.DecimalFormat;
import java.util.*;

import java.util.stream.Collectors;

public class ChiLogger {

    private static final Logger LOG = LoggerFactory.getLogger(ChiLogger.class);

    public static void main (String[] args) throws Exception {

        // Checking input parameters
        final ParameterTool params = ParameterTool.fromArgs(args);
        String kafkaServer;
        String kafkaInputTopic;
        String kafkaOutputTopic;
        String alertWebHook;
        String alertMessage;
        double qValue;

        if(params.has("kafka-server")) {
            kafkaServer = params.get("kafka-server");
        } else {
            LOG.info("Using default Kafka Broker.");
            LOG.info("Use --kafka-server to specify the Kafka Broker.");
            kafkaServer = "kafka.cmdaa.svc.cluster.local:9092";
            //kafkaServer = "kafka-cp-kafka-headless:9092";
        }

        if(params.has("kafka-input-topic")) {
            kafkaInputTopic = params.get("kafka-input-topic");
        } else {
            LOG.info("Using default Kafka Input Topic.");
            LOG.info("Use --kafka-input-topic to specify the Kafka Input Topic.");
            kafkaInputTopic = "fluentd";
        }

        if(params.has("kafka-output-topic")) {
            kafkaOutputTopic = params.get("kafka-output-topic");
        } else {
            LOG.info("Using default Kafka Output Topic.");
            LOG.info("Use --kafka-input-topic to specify the Kafka Output Topic.");
            kafkaOutputTopic = "prometheus-output";
        }

        if(params.has("alert-webhook")) {
            alertWebHook = params.get("alert-webhook");
        } else {
            LOG.info("Using default webhook.");
            LOG.info("Use --alert-webhook to specify the Alert url.");
            alertWebHook = "http://cmdaa-workflows-eventsource-svc.cmdaa.svc.cluster.local:12000/notifications";
        }

        if(params.has("alert-message")) {
            alertMessage = params.get("alert-message");
        } else {
            LOG.info("Using default alert message.");
            LOG.info("Use --alert-message to specify the Alert message.");
            alertMessage = "CMDAA has detected an anomaly in your log messages";
        }

        if(params.has("q-value")) {
            qValue = Double.valueOf(params.get("q-value"));
        } else {
            LOG.info("Using default q-value.");
            LOG.info("Use --q-value to specify the chi-logger threshold value.");
            qValue = .95;
        }

        Properties kafkaProps = new Properties();
        kafkaProps.setProperty("bootstrap.servers", kafkaServer);

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.enableCheckpointing(60 * 1000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);

        KafkaSource<FluentdMessage> source = KafkaSource
                .<FluentdMessage>builder()
                .setBootstrapServers(kafkaServer)
                .setTopics(Arrays.asList(kafkaInputTopic))
                .setGroupId("")
                .setDeserializer(new FluentdRecordDeserializer())
                .setStartingOffsets(OffsetsInitializer.latest())
                //.setBounded(OffsetsInitializer.latest())
                //.setUnbounded(OffsetsInitializer.latest())
                .build();

        FlinkKafkaProducer<Tuple8<String,String, String, Long, Long, Long, Long, String>> kafkaSink = new FlinkKafkaProducer<>(
                kafkaOutputTopic,
                new OutputMessageSerializationSchema(kafkaOutputTopic),
                kafkaProps,
                FlinkKafkaProducer.Semantic.NONE,
                5);

        AlertSink alertSink = new AlertSink(alertWebHook, alertMessage, qValue);

        WatermarkStrategy<FluentdMessage> watermarkStrategy = WatermarkStrategy.<FluentdMessage>forMonotonousTimestamps()
                .withTimestampAssigner(new SerializableTimestampAssigner<FluentdMessage>() {
                    @Override
                    public long extractTimestamp(FluentdMessage message, long l) {
                        return System.currentTimeMillis();
                    }
                });

        DataStream<FluentdMessage> stream = env.fromSource(source, WatermarkStrategy.forMonotonousTimestamps(), "Kafka Source");
        //DataStream<FluentdMessage> generatedSource = env.addSource(UUIDSource.create(100));

        DataStream<Tuple4<String, String, String, Long>> keyedCounts  = stream
                .filter(value -> value.getGrokName() != null)
                //.filter(value -> value.getHostname() != null)
                //.filter(value -> value.getHostname() != "")
                .map(new MapFunction<FluentdMessage, Tuple4<String, String, String, Long>>() {
                    @Override
                    public Tuple4<String, String, String, Long> map(FluentdMessage value) throws Exception {
                        return Tuple4.of(value.getGrokName(), value.getHostname(), value.getSysfile(), 1L);
                    }
                })
//                .assignTimestampsAndWatermarks(new WatermarkStrategy<Tuple2<String, Long>>() {
//                    @Override
//                    public WatermarkGenerator<Tuple2<String, Long>> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
//                        return new WatermarkGenerator<Tuple2<String, Long>>() {
//                            private final long maxTimeLag = 5000; // 5 seconds
//
//                            @Override
//                            public void onEvent(Tuple2<String, Long> event, long eventTimestamp, WatermarkOutput output) {
//                                // don't need to do anything because we work on processing time
//                            }
//
//                            @Override
//                            public void onPeriodicEmit(WatermarkOutput output) {
//                                output.emitWatermark(new Watermark(System.currentTimeMillis()));
//                            }
//                        };
//                    }
//                })
                .keyBy(new KeySelector<Tuple4<String, String, String, Long>, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> getKey(Tuple4<String, String, String, Long> value) throws Exception {
                        return Tuple2.of(value.f0, value.f1);
                    }
                })
                .window(TumblingEventTimeWindows.of(Time.seconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                //.sum(2);
                .reduce((ReduceFunction<Tuple4<String, String, String, Long>>) (data1, data2) -> Tuple4.of(data1.f0, data1.f1, data1.f2, data1.f3 + data2.f3));

        keyedCounts
                .windowAll(TumblingEventTimeWindows.of(Time.seconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                .process(new ProcessWindowFunction(1800, 30, qValue))
                //.addSink(kafkaSink)
                .addSink(alertSink)
                .name("Alert Sink");

        keyedCounts
                .windowAll(TumblingEventTimeWindows.of(Time.seconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                .process(new ProcessWindowFunction(1800, 30, qValue))
                .addSink(kafkaSink)
                .name("Kafka Sink");



       //globalCounts.writeAsText("s3://argo-artifacts/output.txt");

        env.execute("Flink Kafka Chi Log Runner");
    }

    public static class CSVEncoder extends SimpleStringEncoder<Tuple2<String, String>> {

        public CSVEncoder() {
            super();
        }

        @Override
        public void encode(Tuple2<String, String> element, OutputStream stream) throws IOException {
            //super.encode(element, stream);
            String trimmed = element.f0 + "," + element.f1;
            stream.write(trimmed.getBytes(StandardCharsets.UTF_8));
            stream.write(10);
        }

    }


    private static class ProcessWindowFunction extends ProcessAllWindowFunction<Tuple4<String, String, String, Long>,
            Tuple8<String, String, String, Long, Long, Long, Long, String>, TimeWindow> {

        int totalIncrements;

        double qValue = 0.95;

        private ValueState<List> oldKeyMap;

        private ValueState<List> timeSliceStore;

        public ProcessWindowFunction(int totalTime, int timeSlice, double qValue) {
            //Calculate increment threshold
            this.totalIncrements = totalTime / timeSlice;
            this.qValue = qValue;
        }

        @Override
        public void open(Configuration parameters) throws Exception {

            oldKeyMap = getRuntimeContext().getState(
                    new ValueStateDescriptor<>("map", List.class));

            timeSliceStore = getRuntimeContext().getState(
                    new ValueStateDescriptor<>("slices", List.class));
        }

        @Override
        public void process(Context context,
                            Iterable<Tuple4<String, String, String, Long>> iterable,
                            Collector<Tuple8<String, String, String, Long, Long, Long, Long, String>> out) throws Exception {

            if(oldKeyMap.value() == null) {
                //totalCount.update(0L);
                //oldKeyMap.update(initialMapValues(iterable));
                oldKeyMap.update(new ArrayList<Map<Tuple2<String,String>, Long>>());
                //We have no old data yet so there is nothing to compare the time slice data to
            }

            if(timeSliceStore.value() == null) {
                timeSliceStore.update(new ArrayList<Map<Tuple2<String,String>, Long>>());
            }

            //long previousCount = totalCount.value();

            Iterator<Tuple4<String, String, String, Long>> iterator = iterable.iterator();
            long currentCount = sum(iterable);
            Map<Tuple2<String, String>, Long> currentMap = new HashMap<>();
            List<Map<Tuple2<String, String>, Long>> historicList = oldKeyMap.value();
            long oldDataTotalCount = getOldTotalCount(historicList);
            //Map<Tuple2<String, String>, Long> historicMap = oldKeyMap.value();

            while (iterator.hasNext()) {
                Tuple4<String, String, String, Long> tuple = iterator.next();
                long oldKeyCount = 0L;
                Map<Tuple2<String, String>, Long> historicMap = mergeOldDataList(historicList);
                if(historicMap.containsKey(Tuple2.of(tuple.f0, tuple.f1))) {
                    oldKeyCount = sumKey(historicMap, Tuple2.of(tuple.f0, tuple.f1));
                }//                            tuple.f2,

                if(historicList.size() > 0) {
                    String score = getChiSqrLoggerScore(tuple.f3, currentCount, oldKeyCount, oldDataTotalCount);
//                    if (Double.valueOf(score) >= qValue) {
//                        out.collect(Tuple8.of(tuple.f0, tuple.f1, tuple.f2, tuple.f3, currentCount, oldKeyCount, oldDataTotalCount, score));
//                    }
                    out.collect(Tuple8.of(tuple.f0, tuple.f1, tuple.f2, tuple.f3,
                            currentCount, oldKeyCount, oldDataTotalCount, score));
//                    out.collect(Tuple6.of(tuple.f0,
//                            tuple.f2,
//                            currentCount,
//                            oldKeyCount,
//                            getChiSqrLoggerScore(tuple.f3, currentCount, oldKeyCount, oldDataTotalCount),
//                            //tuple.f2 + ", " + currentCount + ", " + previousKeyCount + ", " + previousCount,
//                            oldDataTotalCount));
                }
                currentMap.put(Tuple2.of(tuple.f0, tuple.f1), tuple.f3);
                //keyTotalMap.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
                //out.collect(Tuple3.of(tuple.f0, keyDifference(tuple.f0, iterable), sum(iterable)));
            }
            List<Map<Tuple2<String, String>, Long>> sliceList = timeSliceStore.value();

            if(sliceList.size() == totalIncrements) {
                //Remove the oldest value in the list and add the new one
                sliceList.remove(0);
                sliceList.add(currentMap);
                historicList.remove(0);
                historicList.add(currentMap);
            } else {
                sliceList.add(currentMap);
                historicList.add(currentMap);
            }

            timeSliceStore.update(sliceList);

            //totalCount.update(currentCount);
            //ObjectNode jsonNode = jsonParser.createObjectNode();
            oldKeyMap.update(historicList);
        }
    }

    private static class AlertSink extends RichSinkFunction<Tuple8<String, String, String, Long, Long, Long, Long, String>> {

        private String message;
        private String webhook;
        private double qValue;
        private HttpURLConnection con;

        public AlertSink(String webhook, String message, double qValue) {
            this.webhook = webhook;
            this.message = message;
            this.qValue = qValue;
        }

        @Override
        public void open(Configuration config) throws Exception {
            URL url = new URL (webhook);

            con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");

            con.setDoOutput(true);
        }

        @Override
        public void invoke(Tuple8<String, String, String, Long, Long, Long, Long, String> value, Context context) throws Exception {

            String jsonInputString = "{\"subject\": \"chi-logger-alert\", " +
                    "\"message\": \"Sys File: " + value.f2 + " " + message + "\"" +
                    "}";

            byte[] out = jsonInputString.getBytes(StandardCharsets.UTF_8);

            try {
                if(Double.parseDouble(value.f7) >= qValue) {
                    OutputStream stream = con.getOutputStream();
                    stream.write(out);
                    System.out.println(con.getResponseCode() + " " + con.getResponseMessage());
                }
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            } catch (NumberFormatException e) {
                LOG.error("Number Format Exception");
            }
            finally {
                con.disconnect();
            }
        }
    }


    private static class PushToPostgresWindowFunction extends ProcessAllWindowFunction<Tuple3<String, String, Long>, Tuple3<String, String, Long>, TimeWindow> {

        int totalIncrements;

        private ValueState<List> timeSliceStore;

        public PushToPostgresWindowFunction(int totalTime, int timeSlice) {
            totalIncrements = totalTime / timeSlice;
        }

        @Override
        public void open(Configuration parameters) throws Exception {

            timeSliceStore = getRuntimeContext().getState(
                    new ValueStateDescriptor<>("slices", List.class));
        }

        @Override
        public void process(Context context,
                            Iterable<Tuple3<String, String, Long>> iterable,
                            Collector<Tuple3<String, String, Long>> out) throws Exception {

            if(timeSliceStore.value() == null) {
                timeSliceStore.update(new ArrayList<Map<Tuple2<String,String>, Long>>());
            }

            Iterator<Tuple3<String, String, Long>> iterator = iterable.iterator();

            Map<Tuple2<String, String>, Long> currentMap = new HashMap<>();

            while (iterator.hasNext()) {
                Tuple3<String, String, Long> tuple = iterator.next();

                currentMap.put(Tuple2.of(tuple.f0, tuple.f1), tuple.f2);
                //keyTotalMap.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
                //out.collect(Tuple3.of(tuple.f0, keyDifference(tuple.f0, iterable), sum(iterable)));
            }
            List<Map<Tuple2<String, String>, Long>> sliceList = timeSliceStore.value();

            LOG.info("The total increment is {}", totalIncrements);
            if(sliceList.size() == totalIncrements) {
                //Remove the oldest value in the list and add the new one
                sliceList.remove(0);
                sliceList.add(currentMap);
                Map<Tuple2<String, String>, Long> historicMap = mergeOldDataList(sliceList);

                historicMap.forEach((k, v) -> out.collect(Tuple3.of(k.f0, k.f1, v.longValue())));
            } else {
                sliceList.add(currentMap);
            }

            timeSliceStore.update(sliceList);
            //totalCount.update(currentCount);

        }
    }

    private static class RichChiLoggerInputSink extends RichSinkFunction<Tuple6<String, Long, Long, Long, String, Long>> {

        private String url;

        public RichChiLoggerInputSink(String url) {
            this.url = url;
        }

        private static final String UPSERT_CHI_LOG_MESSAGE  =
                "INSERT INTO public.chi_logger_score (msg_id, count_new, total_new, count_old, score, total_old) "
                        + "VALUES (?, ?, ?, ?, ?, ?) "
                        + "ON CONFLICT (msg_id) "
                        + "DO UPDATE SET count_new = EXCLUDED.count_new, "
                        + "total_new = EXCLUDED.total_new, "
                        + "count_old = EXCLUDED.count_old, "
                        + "score = EXCLUDED.score, "
                        + "total_old = EXCLUDED.total_old";

        private PreparedStatement statement;

        @Override
        public void open(Configuration parameters) throws Exception {
            Class.forName("org.postgresql.Driver");
            Connection connection =
                    DriverManager.getConnection(url);

            statement = connection.prepareStatement(UPSERT_CHI_LOG_MESSAGE);
        }

        @Override
        public void invoke(Tuple6<String, Long, Long, Long, String, Long> message, Context context) throws Exception {

            statement.setString(1, message.f0);
            statement.setLong(2, message.f1);
            statement.setLong(3, message.f2);
            statement.setLong(4, message.f3);
            statement.setString(4, message.f4);
            statement.setLong(5, message.f5);
            statement.addBatch();
            statement.executeBatch();
        }

    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }


        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }

    private static long sum(Iterable<Tuple4<String, String, String, Long>> values) {
        long sum = 0L;
        for (Tuple4<String, String, String, Long> value : values) {
            sum += value.f3;
        }
        return sum;
    }

    private static Map<Tuple2<String, String>, Long> initialMapValues(Iterable<Tuple3<String, String, Long>> values) {
        Map<Tuple2<String, String>, Long> map = new HashMap<>();
        values.forEach(value -> map.put(Tuple2.of(value.f0, value.f1), 0L));
        return map;
    }

    private static long keyDifference(String key, Iterable<Tuple2<String, Long>> tuples) {
        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : tuples) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }

        List<Long> values = map.get(key);

        Collections.sort(values);

        int size = values.size();
        if(size >= 2) {
            return values.get(size - 1) - values.get(size - 2);
        } else {
            return values.get(size - 1);
        }
    }

    private static long sumKeys(Map<Tuple2<String, String>, Long> map) {
        return map.entrySet().stream().mapToLong(l -> l.getValue()).sum();
    }

    private static long sumKey(Map<Tuple2<String, String>, Long> map, Tuple2<String, String> key) {
        return map.get(key).longValue();
    }

    private static Map<Tuple2<String, String>, Long> mergeOldDataList(List<Map<Tuple2<String, String>, Long>> list) {
        return list.stream()
                .flatMap(m -> m.entrySet().stream())
                .collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.summingLong(Map.Entry::getValue)));
    }

    private static long getOldTotalCount(List<Map<Tuple2<String, String>, Long>> list) {
        return list.stream()
                .flatMap(m -> m.entrySet().stream())
                .collect(Collectors.summingLong(Map.Entry::getValue));
    }

    /**
     * Calculates the cumulative probability of the word occurring in the document collection
     *
     * @param countNew   number of times word occurs in the document
     * @param totalNew total number of words in the document
     * @param countOld   number of times word occurs in document collection
     * @param totalOld total number of words in the document collection
     * @return the cumulative probability of the word occurring in the document collection
     */
    private static String getChiSqrLoggerScore(long countNew, long totalNew, long countOld, long totalOld) {

        DecimalFormat df = new DecimalFormat("#.#####");
        df.setMaximumFractionDigits(5);
        double dblCountNew = Long.valueOf(countNew).doubleValue();
        double dblTotalNew = Long.valueOf(totalNew).doubleValue();
        double dblCountOld = Long.valueOf(countOld).doubleValue();
        double dblTotalOld = Long.valueOf(totalOld).doubleValue();

        ChiSquaredDistribution chi = new ChiSquaredDistribution(1d);
        double p = dblCountNew / dblTotalNew;
        double q = dblCountOld / dblTotalOld;
        if (p < q) return df.format(0d);
        double t = (dblCountNew + dblTotalOld) / (dblTotalNew + dblTotalOld);
        if (t == 0) {
            System.out.println("attempted division by zero.");
            return df.format(0d);
        }
        double v = dblCountNew * Math.log(p / t) + dblCountOld * Math.log(q / t);
        if (t == 1) return df.format(2d * v);
        if (p < 1) v += (dblTotalNew - dblCountNew) * Math.log((1 - p) / (1 - t));
        if (q < 1) v += (dblTotalOld - dblCountOld) * Math.log((1 - q) / (1 - t));
        v += v;

        return df.format(1d - chi.cumulativeProbability(v));
    }


    private static class UUIDSource implements SourceFunction<FluentdMessage> {

        private static final long serialVersionUID = 1L;
        private Integer[] uuids;

        private UUID uuid;

        private volatile boolean isRunning = true;

        private UUIDSource(int number) {
            uuids = new Integer[number];
        }

        public static UUIDSource create(int number) {
            return new UUIDSource(number);
        }

        @Override
        public void run(SourceContext<FluentdMessage> ctx) throws Exception {

            while (isRunning) {

                Thread.sleep(20 * 1000);
                FluentdMessage message1 = new FluentdMessage();
                message1.setGrokName("d56658a7-7566-40c6-822a-d3ac60feab1a");
                message1.setHostname("dannunzio");
                FluentdMessage message2 = new FluentdMessage();
                message2.setGrokName("606bb0a6-5a64-3eac-9635-6424dfccdc68");
                message2.setHostname("dannunzio");
                FluentdMessage message3 = new FluentdMessage();
                message3.setGrokName("ad7a6946-2b65-490d-a510-a6e2568f3c0e");
                message3.setHostname("dannunzio");
                FluentdMessage message4 = new FluentdMessage();
                message4.setHostname("spackler");

                ctx.collectWithTimestamp(message1, System.currentTimeMillis());
                ctx.collectWithTimestamp(message2, System.currentTimeMillis());
                ctx.collectWithTimestamp(message3, System.currentTimeMillis());
                ctx.collectWithTimestamp(message4, System.currentTimeMillis());
                ctx.emitWatermark(new org.apache.flink.streaming.api.watermark.Watermark(System.currentTimeMillis()));
                //                for (int i = 0; i < uuids.length; i++) {
                //                    uuid = UUID.randomUUID();
                //
                //                    Tuple2<String, Long> record = new Tuple2<>(String.valueOf(uuid), 1L);
                //                    ctx.collect(record);
                //                    Thread.sleep(5 * 1000);
                //                    ctx.collect(record);
                //                }
            }
        }

        @Override
        public void cancel() {
            isRunning = false;
        }
    }



}
