package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;


import com.ibm.icu.impl.number.SimpleModifier;
import io.cmdaa.streaming.java.kafka.model.LogDataTable;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.LogDataTableDeserializer;

import org.apache.calcite.DataContext;
import org.apache.calcite.adapter.java.JavaTypeFactory;
import org.apache.calcite.linq4j.QueryProvider;
import org.apache.calcite.rel.core.Join;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.sink.filesystem.BucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.SimpleVersionedStringSerializer;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.types.Row;
import org.apache.flink.types.RowKind;
import org.apache.flink.util.CloseableIterator;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.calcite.runtime.SqlFunctions;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import java.util.Arrays;
import java.util.UUID;

public class ChiLogRunnerTestLogLookup {

    private static Logger logger = LoggerFactory.getLogger(ChiLogRunnerTestLogLookup.class);

    public static void main(String[] args) throws Exception {

        final ParameterTool params = ParameterTool.fromArgs(args);

        final String kafkaServer;
        final String kafkaInputTopic;
        final String kafkaFailuresTopic;

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        final StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        env.enableCheckpointing(60 * 1000);
        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.getCheckpointConfig().setCheckpointTimeout(3 * 60 * 1000);

        if(params.has("kafka-server")) {
            kafkaServer = params.get("kafka-server");
        } else {
            kafkaServer = "kafka.cmdaa.svc.cluster.local:9092";
            //kafkaServer = "kafka-cp-kafka-headless:9092";
        }

        if(params.has("kafka-input-topic")) {
            kafkaInputTopic = params.get("kafka-input-topic");
        } else {
            kafkaInputTopic = "fluentd";
        }

        if(params.has("kafka-failures-topic")) {
            kafkaFailuresTopic = params.get("kafka-failures-topic");
        } else {
            kafkaFailuresTopic = "grokfailures";
        }

        KafkaSource<LogDataTable> logTableSource = buildConnection(kafkaFailuresTopic,
                kafkaServer, new LogDataTableDeserializer());

        KafkaSource<FluentdMessage> dataSource = buildConnection(kafkaInputTopic,
                kafkaServer, new FluentdRecordDeserializer());

//        KafkaSource<LogDataTable> logTableSource = KafkaSource
//                .<LogDataTable>builder()
//                .setBootstrapServers(kafkaServer)
//                .setTopics(Arrays.asList(kafkaFailuresTopic))
//                .setGroupId("")
//                .setDeserializer(new LogDataTableDeserializer())
//                //.setStartingOffsets(OffsetsInitializer.earliest())
//                //.setBounded(OffsetsInitializer.latest())
//                //.setUnbounded(OffsetsInitializer.latest())
//                .build();

//        KafkaSource<FluentdMessage> dataSource = KafkaSource
//                .<FluentdMessage>builder()
//                .setBootstrapServers(kafkaServer)
//                .setTopics(Arrays.asList(kafkaInputTopic))
//                .setGroupId("")
//                .setDeserializer(new FluentdRecordDeserializer())
//                //.setStartingOffsets(OffsetsInitializer.earliest())
//                //.setBounded(OffsetsInitializer.latest())
//                //.setUnbounded(OffsetsInitializer.latest())
//                .build();

        DataStream<FluentdMessage> stream = env.fromSource(dataSource,
                WatermarkStrategy.forMonotonousTimestamps(), "Kafka Source");
        DataStream<LogDataTable> logTableStream = env.fromSource(logTableSource,
                WatermarkStrategy.forMonotonousTimestamps(), "Log Table Source");

        final Table tableA = tableEnv.fromDataStream(stream);

        tableEnv.createTemporaryView("LookupTable", logTableStream);

        final Table result =
                tableEnv.sqlQuery(
                        "SELECT a.logId, t.s3Path, a.message, a.hostname, MAX(t.insertTime) as insertTime " +
                                "FROM " +
                                tableA  +
                                " AS a " +
                                    "JOIN LookupTable AS t " +
                                    "ON t.logId = a.logId " +
                                "WHERE a.grokFailure = 'No grok pattern matched' " +
                                "GROUP BY a.logId, t.s3Path, a.message, a.hostname"
                );

        DataStream<JoinedRow> joinedStream = tableEnv.toDataStream(result, JoinedRow.class);

        OutputFileConfig config = OutputFileConfig
                .builder()
                .withPartPrefix("cmdaa")
                .withPartSuffix(".log")
                .build();

        final StreamingFileSink<JoinedRow> sink =
                StreamingFileSink.forRowFormat(
                                //new Path("s3a://flink.cmdaa/hadoop_2k"),
                                new Path("s3://cmdaa/"),
                                new CSVEncoder())
                        .withBucketAssigner(new KeyBucketAssigner())
//                        .withRollingPolicy(
//                                DefaultRollingPolicy.builder()
//                                .withRolloverInterval(TimeUnit.MINUTES.toMillis(2))
//                                .withInactivityInterval(TimeUnit.MINUTES.toMillis(5))
//                                .withMaxPartSize(1024 * 1024 * 1024)
//                                .build())SinkFunction.super.invoke(value, context);
                        .withRollingPolicy(OnCheckpointRollingPolicy.build())
                        .withOutputFileConfig(config)
                        .build();

        joinedStream.addSink(sink);

//        DataStream<Tuple4<String, String, String, String>> resultStream = logTableStream
//                .map(new MapFunction<LogDataTable, Tuple4<String, String, String, String>>() {
//                    @Override
//                    public Tuple4<String, String, String, String> map(LogDataTable row) throws Exception {
//                        return Tuple4.of(row.getLogName(), row.getS3Path(), row.getSystemFile(), row.getS3Bucket());
//                    }
//                });
        //resultStream.addSink(sink);

        // logLookup.forEach(System.out::println);
        //resultStream.writeAsText("s3://argo-artifacts/output.txt");
        env.execute("Streaming Join with Lookup");

    }

    public static class CSVEncoder extends SimpleStringEncoder<JoinedRow> {

        public CSVEncoder() {
            super();
        }

        @Override
        public void encode(JoinedRow element, OutputStream stream) throws IOException {
            //super.encode(element, stream);
            String trimmed = element.message;
            stream.write(trimmed.getBytes(StandardCharsets.UTF_8));
            stream.write(10);
        }
    }

    /** Use first field for buckets. */
    public static final class KeyBucketAssigner implements BucketAssigner<JoinedRow, String> {

        private static final long serialVersionUID = 987325769970523326L;

        @Override
        public String getBucketId(final JoinedRow element, final Context context) {
//            Row result = logLookup
//                    .stream()
//                    .filter(r -> r.getField(0).equals(element.getField(1)))
//                    .findAny()
//                    .orElse(null);
//            return result.getField(1).toString();
            return removeLastCharOptional(element.s3Path);
        }

        @Override
        public SimpleVersionedSerializer<String> getSerializer() {
            return SimpleVersionedStringSerializer.INSTANCE;
        }
    }

    private static <E, T extends KafkaRecordDeserializationSchema<E>> KafkaSource<E> buildConnection (String topic,
                                                                                                   String bootstrapServer,
                                                                                                   T deserializer) {
        return KafkaSource
                .<E>builder()
                .setBootstrapServers(bootstrapServer)
                .setTopics(Arrays.asList(topic))
                .setGroupId("")
                .setDeserializer(deserializer)
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //.setBounded(OffsetsInitializer.latest())
                //.setUnbounded(OffsetsInitializer.latest())
                .build();
    }

    /** Simple POJO. */
    public static class LogLookup {
        public Long logId;
        public String path;

        // for POJO detection in DataStream API
        public LogLookup() {}

        // for structured type detection in Table API
        public LogLookup(Long logId, String path) {
            this.logId = logId;
            this.path = path;
        }

        @Override
        public String toString() {
            return "LogLookup{"
                    + "log_id="
                    + logId
                    + ", path='"
                    + path
                    + "'"
                    + '}';
        }
    }

    /** Simple POJO. */
    public static class StreamRow {
        public Long logId;
        public String message;
        public String grokName;
        public String hostname;

        // for POJO detection in DataStream API
        public StreamRow() {}

        // for structured type detection in Table API
        public StreamRow(Long logId, String message, String grokName, String hostname) {
            this.logId = logId;
            this.message = message;
            this.hostname = hostname;
            this.grokName = grokName;
        }

        @Override
        public String toString() {
            return "StreamRow{"
                    + "log_id="
                    + logId
                    + ", message='"
                    + message
                    + "'"
                    + '}';
        }
    }

    /** Simple POJO. */
    public static class JoinedRow {
        public String logId;
        public String message;
        public String s3Path;
        public String hostname;
        public Long insertTime;

        // for POJO detection in DataStream API
        public JoinedRow() {}

        // for structured type detection in Table API
        public JoinedRow(String logId, String message, String s3Path, String hostname, Long insertTime) {
            this.logId = logId;
            this.message = message;
            this.hostname = hostname;
            this.s3Path = s3Path;
            this.insertTime = insertTime;
        }

        @Override
        public String toString() {
            return "JoinedRow{"
                    + "log_id="
                    + logId
                    + ", message='"
                    + message
                    + "'"
                    + ", path='"
                    + s3Path
                    + "'"
                    + '}';
        }
    }

    public static String removeLastCharOptional(String s) {
        return Optional.ofNullable(s)
                .filter(str -> str.length() != 0)
                .map(str -> str.substring(0, str.lastIndexOf('/')))
                .orElse(s);
    }

    private static class LogDataTableDeserializer implements KafkaRecordDeserializationSchema<LogDataTable> {

        static ObjectMapper objectMapper;

        static {
//            SimpleModule simpleModule = new SimpleModule("LogDeserializer");
//            simpleModule.addDeserializer(LogDataTable.class, new io.cmdaa.streaming.java.kafka.schema.LogDataTableDeserializer(LogDataTable.class));
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }

        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<LogDataTable> out) throws IOException {
            logger.info("LogDataTable = " + consumerRecord.value());
            out.collect(objectMapper.readValue(consumerRecord.value(), LogDataTable.class));
        }

        @Override
        public TypeInformation<LogDataTable> getProducedType() {
            return TypeInformation.of(LogDataTable.class);
        }
    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }

        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }



    private static class UUIDSource implements SourceFunction<FluentdMessage> {

        private static final long serialVersionUID = 1L;
        private Integer[] uuids;

        private UUID uuid;

        private volatile boolean isRunning = true;

        private UUIDSource(int number) {
            uuids = new Integer[number];
        }

        public static UUIDSource create(int number) {
            return new UUIDSource(number);
        }

        public void run(SourceContext<FluentdMessage> ctx) throws Exception {

            while (isRunning) {
//                Thread.sleep(20 * 1000);
//                FluentdMessage message1 = new FluentdMessage();
//                message1.setGrokName("d56658a7-7566-40c6-822a-d3ac60feab1a");
//                //message1.setHostname("dannunzio");
//                FluentdMessage message2 = new FluentdMessage();
//                message2.setGrokName("606bb0a6-5a64-3eac-9635-6424dfccdc68");
//                //message2.setHostname("dannunzio");
//                FluentdMessage message3 = new FluentdMessage();
//                message3.setGrokName("ad7a6946-2b65-490d-a510-a6e2568f3c0e");
//                //message3.setHostname("dannunzio");
//
//                ctx.collectWithTimestamp(message1, System.currentTimeMillis());
//                ctx.collectWithTimestamp(message2, System.currentTimeMillis());
//                ctx.collectWithTimestamp(message3, System.currentTimeMillis());

                for (int i = 0; i < uuids.length; i++) {
                    uuid = UUID.randomUUID();

                    FluentdMessage record = new FluentdMessage();
                    record.setGrokName(uuid.toString());
                    record.setHostname("unmatched_01");
                    ctx.collectWithTimestamp(record, System.currentTimeMillis());
                    Thread.sleep(10);
                    ctx.collectWithTimestamp(record, System.currentTimeMillis());
                    ctx.emitWatermark(new Watermark(System.currentTimeMillis()));
                }
            }
        }

        @Override
        public void cancel() {
            isRunning = false;
        }
    }

}
