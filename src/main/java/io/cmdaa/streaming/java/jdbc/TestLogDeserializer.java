package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cmdaa.streaming.java.kafka.model.LogDataTable;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.BucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.SimpleVersionedStringSerializer;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class TestLogDeserializer {

    public static void main(String[] args) throws Exception {

        final String kafkaServer = "kafka.cmdaa.svc.cluster.local:9092";

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //final StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        env.enableCheckpointing(60 * 1000);
        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.getCheckpointConfig().setCheckpointTimeout(60000);

        KafkaSource<LogDataTable> logTableSource = KafkaSource
                .<LogDataTable>builder()
                .setBootstrapServers(kafkaServer)
                .setTopics(Arrays.asList("test"))
                .setGroupId("")
                .setDeserializer(new LogDataTableDeserializer())
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //.setBounded(OffsetsInitializer.latest())
                .setUnbounded(OffsetsInitializer.latest())
                .build();

        DataStream<LogDataTable> logTableStream = env.fromSource(logTableSource,
                WatermarkStrategy.forMonotonousTimestamps(), "Log Table Source");

        OutputFileConfig config = OutputFileConfig
                .builder()
                .withPartPrefix("cmdaa")
                .withPartSuffix(".csv")
                .build();

        final StreamingFileSink<LogDataTable> sink =
                StreamingFileSink.forRowFormat(
                                //new Path("s3a://flink.cmdaa/hadoop_2k"),
                                new Path("s3://argo-artifacts/files"),
                                new CSVEncoder())
                        .withBucketAssigner(new KeyBucketAssigner())
//                        .withRollingPolicy(
//                                DefaultRollingPolicy.builder()
//                                .withRolloverInterval(TimeUnit.MINUTES.toMillis(2))
//                                .withInactivityInterval(TimeUnit.MINUTES.toMillis(5))
//                                .withMaxPartSize(1024 * 1024 * 1024)
//                                .build())
                        .withRollingPolicy(OnCheckpointRollingPolicy.build())
                        .withOutputFileConfig(config)
                        .build();

        logTableStream.addSink(sink);

        env.execute("Streaming Join with Lookup");

    }

    public static class CSVEncoder extends SimpleStringEncoder<LogDataTable> {

        public CSVEncoder() {
            super();
        }

        @Override
        public void encode(LogDataTable element, OutputStream stream) throws IOException {
            //super.encode(element, stream);
            String trimmed = element.getS3Path();
            stream.write(trimmed.getBytes(StandardCharsets.UTF_8));
            stream.write(10);
        }
    }


    /** Use first field for buckets. */
    public static final class KeyBucketAssigner implements BucketAssigner<LogDataTable, String> {

        private static final long serialVersionUID = 987325769970523326L;

        @Override
        public String getBucketId(final LogDataTable element, final Context context) {
//            Row result = logLookup
//                    .stream()
//                    .filter(r -> r.getField(0).equals(element.getField(1)))
//                    .findAny()
//                    .orElse(null);
//            return result.getField(1).toString();
            return element.getS3Bucket();
        }

        @Override
        public SimpleVersionedSerializer<String> getSerializer() {
            return SimpleVersionedStringSerializer.INSTANCE;
        }
    }

    private static class LogDataTableDeserializer implements KafkaRecordDeserializationSchema<LogDataTable> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }

        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<LogDataTable> out) throws IOException {
            out.collect(objectMapper.readValue(consumerRecord.value(), LogDataTable.class));
        }

        @Override
        public TypeInformation<LogDataTable> getProducedType() {
            return TypeInformation.of(LogDataTable.class);
        }
    }


}
