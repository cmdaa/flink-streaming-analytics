package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.PurgingTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;

import org.apache.flink.api.java.utils.ParameterTool;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class ChiLogRunnerStreaming {


    public static void main (String[] args) throws Exception {

        // Checking input parameters
        final ParameterTool params = ParameterTool.fromArgs(args);
        String kafkaServer;

        if(params.has("kafka-server")) {
            kafkaServer = params.get("kafka-server");
        } else {
            System.out.println("Using default Kafka Broker.");
            System.out.println("Use --kafka-server to specify the Kafka Broker.");
            kafkaServer = "kafka-cp-kafka-headless:9092";
        }

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        KafkaSource<FluentdMessage> source = KafkaSource
                .<FluentdMessage>builder()
                .setBootstrapServers(kafkaServer)
                .setTopics(Arrays.asList("fluentd"))
                .setDeserializer(new FluentdRecordDeserializer())
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //.setBounded(OffsetsInitializer.latest())
                .setUnbounded(OffsetsInitializer.latest())
                .build();

        WatermarkStrategy<FluentdMessage> watermarkStrategy = WatermarkStrategy.<FluentdMessage>forMonotonousTimestamps()
                .withTimestampAssigner(new SerializableTimestampAssigner<FluentdMessage>() {
                    @Override
                    public long extractTimestamp(FluentdMessage message, long l) {
                        return System.currentTimeMillis();
                    }
                });

        DataStream<FluentdMessage> stream = env.fromSource(source, WatermarkStrategy.forMonotonousTimestamps(), "Kafka Source");
       //DataStream<FluentdMessage> generatedSource = env.addSource(UUIDSource.create(5));

        DataStream<Tuple3<String, String, Long>> keyedCounts  = stream
                .filter(value -> value.getGrokName() != null)
                .filter(value -> value.getHostname() != null)
                .filter(value -> value.getHostname() != "")
                .map(new MapFunction<FluentdMessage, Tuple3<String, String, Long>>() {
                    @Override
                    public Tuple3<String, String, Long> map(FluentdMessage value) throws Exception {
                        return Tuple3.of(value.getGrokName(), value.getHostname(), 1L);
                    }
                })
//                .assignTimestampsAndWatermarks(new WatermarkStrategy<Tuple2<String, Long>>() {
//                    @Override
//                    public WatermarkGenerator<Tuple2<String, Long>> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
//                        return new WatermarkGenerator<Tuple2<String, Long>>() {
//                            private final long maxTimeLag = 5000; // 5 seconds
//
//                            @Override
//                            public void onEvent(Tuple2<String, Long> event, long eventTimestamp, WatermarkOutput output) {
//                                // don't need to do anything because we work on processing time
//                            }
//
//                            @Override
//                            public void onPeriodicEmit(WatermarkOutput output) {
//                                output.emitWatermark(new Watermark(System.currentTimeMillis()));
//                            }
//                        };
//                    }
//                })
                .keyBy(new KeySelector<Tuple3<String, String, Long>, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> getKey(Tuple3<String, String, Long> value) throws Exception {
                        return Tuple2.of(value.f0, value.f1);
                    }
                })
                .window(TumblingEventTimeWindows.of(Time.seconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                //.sum(2);
                .reduce((ReduceFunction<Tuple3<String, String, Long>>) (data1, data2) -> Tuple3.of(data1.f0, data1.f1, data1.f2 + data2.f2));


        DataStream<Tuple5<String, String, Long, String, Long>> globalCounts = keyedCounts
                .windowAll(TumblingEventTimeWindows.of(Time.seconds(30)))
                .trigger(PurgingTrigger.of(EventTimeTrigger.create()))
                .process(new ProcessAllWindowFunction<Tuple3<String, String, Long>, Tuple5<String, String, Long, String, Long>, TimeWindow>() {

                    private ValueState<Long> totalCount;
                    private ValueState<Map> keyMap;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        totalCount = getRuntimeContext().getState(
                                new ValueStateDescriptor<>("count", Long.class));

                        keyMap = getRuntimeContext().getState(
                                new ValueStateDescriptor<>("map", Map.class));
                    }

                    @Override
                    public void process(Context context,
                                        Iterable<Tuple3<String, String, Long>> iterable,
                                        Collector<Tuple5<String, String, Long, String, Long>> out) throws Exception {

                        if(totalCount.value() == null) {
                            totalCount.update(0L);
                            keyMap.update(initialMapValues(iterable));
                        }

                        long previousCount = totalCount.value();

                        Iterator<Tuple3<String, String, Long>> iterator = iterable.iterator();
                        long currentCount = sum(iterable);
                        Map<Tuple2<String, String>, Long> currentMap = new HashMap<>();
                        Map<Tuple2<String, String>, Long> previousMap = keyMap.value();
                        //Carry forward keys from previous sessions
                        currentMap.putAll(previousMap);

                        while (iterator.hasNext()) {
                            Tuple3<String, String, Long> tuple = iterator.next();
                            long previousKeyCount = 0L;
                            if(previousMap.containsKey(Tuple2.of(tuple.f0, tuple.f1))) {
                                previousKeyCount = sumKey(previousMap, Tuple2.of(tuple.f0, tuple.f1));
                            }
                            out.collect(Tuple5.of(tuple.f0,
                                    tuple.f1,
                                    tuple.f2,
                                    getChiSqrLoggerScore(tuple.f2, currentCount, previousKeyCount, previousCount),
                                    //tuple.f2 + ", " + currentCount + ", " + previousKeyCount + ", " + previousCount,
                                    currentCount));
                            currentMap.put(Tuple2.of(tuple.f0, tuple.f1), tuple.f2);
                            //keyTotalMap.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
                            //out.collect(Tuple3.of(tuple.f0, keyDifference(tuple.f0, iterable), sum(iterable)));
                        }

                        totalCount.update(currentCount);
                        keyMap.update(currentMap);
                        out.collect(Tuple5.of(null, null, null, null, null));
                    }
                })
                //.addSink(new RichChiLoggerInputSink())
                .name("Postgres Sink");

        globalCounts.writeAsText("s3://argo-artifacts/output.txt");

        env.execute("Flink Kafka Chi Log Runner");

    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }


        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }

    private static long sum(Iterable<Tuple3<String, String, Long>> values) {
        long sum = 0L;
        for (Tuple3<String, String, Long> value : values) {
            sum += value.f2;
        }
        return sum;
    }

    private static Map<Tuple2<String, String>, Long> initialMapValues(Iterable<Tuple3<String, String, Long>> values) {
        Map<Tuple2<String, String>, Long> map = new HashMap<>();
        values.forEach(value -> map.put(Tuple2.of(value.f0, value.f1), 0L));
        return map;
    }

    private static long keyDifference(String key, Iterable<Tuple2<String, Long>> tuples) {
        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : tuples) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }

        List<Long> values = map.get(key);

        Collections.sort(values);

        int size = values.size();
        if(size >= 2) {
            return values.get(size - 1) - values.get(size - 2);
        } else {
            return values.get(size - 1);
        }
    }

    private static long sumKeys(Map<Tuple2<String, String>, Long> map) {
        return map.entrySet().stream().mapToLong(l -> l.getValue()).sum();
    }

    private static long sumKey(Map<Tuple2<String, String>, Long> map, Tuple2<String, String> key) {
        return map.get(key).longValue();
    }

    /**
     * Calculates the cumulative probability of the word occurring in the document collection
     *
     * @param countNew   number of times word occurs in the document
     * @param totalNew total number of words in the document
     * @param countOld   number of times word occurs in document collection
     * @param totalOld total number of words in the document collection
     * @return the cumulative probability of the word occurring in the document collection
     */
    private static String getChiSqrLoggerScore(long countNew, long totalNew, long countOld, long totalOld) {

        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(5);
        double dblCountNew = new Long(countNew).doubleValue();
        double dblTotalNew = new Long(totalNew).doubleValue();
        double dblCountOld = new Long(countOld).doubleValue();
        double dblTotalOld = new Long(totalOld).doubleValue();

        ChiSquaredDistribution chi = new ChiSquaredDistribution(1d);
        double p = dblCountNew / dblTotalNew;
        double q = dblCountOld / dblTotalOld;
        if (p < q) return df.format(0d);
        double t = (dblCountNew + dblTotalOld) / (dblTotalNew + dblTotalOld);
        if (t == 0) {
            System.out.println("attempted division by zero.");
            return df.format(0d);
        }
        double v = dblCountNew * Math.log(p / t) + dblCountOld * Math.log(q / t);
        if (t == 1) return df.format(2d * v);
        if (p < 1) v += (dblTotalNew - dblCountNew) * Math.log((1 - p) / (1 - t));
        if (q < 1) v += (dblTotalOld - dblCountOld) * Math.log((1 - q) / (1 - t));
        v += v;

        return df.format(1d - chi.cumulativeProbability(v));
    }


    // *************************************************************************
    // USER FUNCTIONS
    // *************************************************************************

    private static class UUIDSource implements SourceFunction<FluentdMessage> {

        private static final long serialVersionUID = 1L;
        private Integer[] uuids;

        private UUID uuid;

        private volatile boolean isRunning = true;

        private UUIDSource(int number) {
            uuids = new Integer[number];
        }

        public static UUIDSource create(int number) {
            return new UUIDSource(number);
        }

        @Override
        public void run(SourceContext<FluentdMessage> ctx) throws Exception {

            while (isRunning) {
                Thread.sleep(20 * 1000);
                FluentdMessage message1 = new FluentdMessage();
                message1.setGrokName("d56658a7-7566-40c6-822a-d3ac60feab1a");
                message1.setHostname("dannunzio");
                FluentdMessage message2 = new FluentdMessage();
                message2.setGrokName("606bb0a6-5a64-3eac-9635-6424dfccdc68");
                message2.setHostname("dannunzio");
                FluentdMessage message3 = new FluentdMessage();
                message3.setGrokName("ad7a6946-2b65-490d-a510-a6e2568f3c0e");
                message3.setHostname("dannunzio");

                ctx.collectWithTimestamp(message1, System.currentTimeMillis());
                ctx.collectWithTimestamp(message2, System.currentTimeMillis());
                ctx.collectWithTimestamp(message3, System.currentTimeMillis());
                ctx.emitWatermark(new org.apache.flink.streaming.api.watermark.Watermark(System.currentTimeMillis()));
    //                for (int i = 0; i < uuids.length; i++) {
    //                    uuid = UUID.randomUUID();
    //
    //                    Tuple2<String, Long> record = new Tuple2<>(String.valueOf(uuid), 1L);
    //                    ctx.collect(record);
    //                    Thread.sleep(5 * 1000);
    //                    ctx.collect(record);
    //                }
            }
        }

        @Override
        public void cancel() {
            isRunning = false;
        }
    }



}
