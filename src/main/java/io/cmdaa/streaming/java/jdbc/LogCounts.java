package io.cmdaa.streaming.java.jdbc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.LogOutputMessageSerializationSchema;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

public class LogCounts {

    private static final Logger LOG = LoggerFactory.getLogger(LogCounts.class);

    public static void main(String[] args) throws Exception {


        // Checking input parameters
        final ParameterTool params = ParameterTool.fromArgs(args);
        String kafkaServer;
        String kafkaInputTopic;
        String kafkaOutputTopic;

        if(params.has("kafka-server")) {
            kafkaServer = params.get("kafka-server");
        } else {
            LOG.info("Using default Kafka Broker.");
            LOG.info("Use --kafka-server to specify the Kafka Broker.");
            kafkaServer = "kafka.cmdaa.svc.cluster.local:9092";
            //kafkaServer = "kafka-cp-kafka-headless:9092";
        }

        if(params.has("kafka-input-topic")) {
            kafkaInputTopic = params.get("kafka-input-topic");
        } else {
            LOG.info("Using default Kafka Input Topic.");
            LOG.info("Use --kafka-input-topic to specify the Kafka Input Topic.");
            kafkaInputTopic = "fluentd";
        }

        if(params.has("kafka-output-topic")) {
            kafkaOutputTopic = params.get("kafka-output-topic");
        } else {
            LOG.info("Using default Kafka Output Topic.");
            LOG.info("Use --kafka-input-topic to specify the Kafka Output Topic.");
            kafkaOutputTopic = "metrics_application";
        }

        Properties kafkaProps = new Properties();
        kafkaProps.setProperty("bootstrap.servers", kafkaServer);

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.enableCheckpointing(60 * 1000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);

        KafkaSource<FluentdMessage> source = KafkaSource
                .<FluentdMessage>builder()
                .setBootstrapServers(kafkaServer)
                .setTopics(Arrays.asList(kafkaInputTopic))
                .setGroupId("")
                .setDeserializer(new FluentdRecordDeserializer())
                .setStartingOffsets(OffsetsInitializer.latest())
                //.setBounded(OffsetsInitializer.latest())
                //.setUnbounded(OffsetsInitializer.latest())
                .build();

        FlinkKafkaProducer<Tuple4<String, String, String, Long>> kafkaSink = new FlinkKafkaProducer<>(
                kafkaOutputTopic,
                new LogOutputMessageSerializationSchema(kafkaOutputTopic),
                kafkaProps,
                FlinkKafkaProducer.Semantic.NONE,
                5);

        DataStream<FluentdMessage> stream = env.fromSource(source, WatermarkStrategy.forMonotonousTimestamps(), "Kafka Source");
        //DataStream<FluentdMessage> generatedSource = env.addSource(UUIDSource.create(100000));

        stream
                .filter(value -> value.getGrokName() != null)
                .map(new MapFunction<FluentdMessage, Tuple4<String, String, String, Long>>() {
                    @Override
                    public Tuple4<String, String, String, Long> map(FluentdMessage value) throws Exception {
                        return Tuple4.of(value.getGrokName(), value.getHostname(), value.getLogId(), 1L);
                    }
                })
                .keyBy(value -> value.f0)
                .sum(3)
                .addSink(kafkaSink)
                .setParallelism(1);

        env.execute("Prometheus Count By Log Id");

    }

    private static class FluentdRecordDeserializer implements KafkaRecordDeserializationSchema<FluentdMessage> {

        static ObjectMapper objectMapper;

        static {
            objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }


        @Override
        public void deserialize(ConsumerRecord<byte[], byte[]> consumerRecord, Collector<FluentdMessage> out) throws IOException {

            out.collect(objectMapper.readValue(consumerRecord.value(), FluentdMessage.class));
        }

        @Override
        public TypeInformation<FluentdMessage> getProducedType() {
            return TypeInformation.of(FluentdMessage.class);
        }
    }


    private static class UUIDSource implements SourceFunction<FluentdMessage> {

        private static final long serialVersionUID = 1L;
        private Integer[] uuids;

        private UUID uuid;

        private volatile boolean isRunning = true;

        private UUIDSource(int number) {
            uuids = new Integer[number];
        }

        public static UUIDSource create(int number) {
            return new UUIDSource(number);
        }

        public void run(SourceContext<FluentdMessage> ctx) throws Exception {

            while (isRunning) {
//                Thread.sleep(20 * 1000);
//                FluentdMessage message1 = new FluentdMessage();
//                message1.setGrokName("d56658a7-7566-40c6-822a-d3ac60feab1a");
//                //message1.setHostname("dannunzio");
//                FluentdMessage message2 = new FluentdMessage();
//                message2.setGrokName("606bb0a6-5a64-3eac-9635-6424dfccdc68");
//                //message2.setHostname("dannunzio");
//                FluentdMessage message3 = new FluentdMessage();
//                message3.setGrokName("ad7a6946-2b65-490d-a510-a6e2568f3c0e");
//                //message3.setHostname("dannunzio");
//
//                ctx.collectWithTimestamp(message1, System.currentTimeMillis());
//                ctx.collectWithTimestamp(message2, System.currentTimeMillis());
//                ctx.collectWithTimestamp(message3, System.currentTimeMillis());

                for (int i = 0; i < uuids.length; i++) {
                    uuid = UUID.randomUUID();
                    Thread.sleep(20 * 1000);
                    FluentdMessage record = new FluentdMessage();
                    record.setLogId(uuid.toString());
                    record.setHostname("hostname");
                    record.setSysfile("sysfile");
                    ctx.collectWithTimestamp(record, System.currentTimeMillis());
                    //ctx.collectWithTimestamp(record, System.currentTimeMillis());
                    ctx.emitWatermark(new Watermark(System.currentTimeMillis()));
                }
            }
        }

        @Override
        public void cancel() {
            isRunning = false;
        }
    }

}
