package io.cmdaa.streaming.java.jdbc;

import io.cmdaa.streaming.java.jdbc.function.RichChiLoggerInputSink;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.InputMessageDeserializationSchema;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;

import org.apache.flink.streaming.api.windowing.assigners.*;
import org.apache.flink.streaming.api.windowing.triggers.ProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.PurgingTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import org.apache.kafka.common.serialization.StringDeserializer;

import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;

//import org.apache.flink.connector.kafka.source.KafkaSource;
//import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
//import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializer;
//import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
//
//import org.apache.flink.api.common.RuntimeExecutionMode;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.eventtime.*;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import org.apache.flink.util.Collector;

import java.text.DecimalFormat;
import java.util.Properties;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.StreamSupport;
import java.util.Arrays;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChiLogRunner {

    private static Logger logger = LoggerFactory.getLogger(ChiLogRunner.class);

    private static TypeInformation<Tuple3<String, Long, Long>> typeInfo
            = TypeInformation.of(new TypeHint<Tuple3<String, Long, Long>>(){});

    public static void main(String[] args) throws Exception {

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "kafka-cp-kafka-headless:9092");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        //env.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime);

        DataStream<FluentdMessage> stream = env
                .addSource(getConsumer(properties))
                .assignTimestampsAndWatermarks(WatermarkStrategy.<FluentdMessage>forMonotonousTimestamps()
                        .withTimestampAssigner(new SerializableTimestampAssigner<FluentdMessage>() {
                            @Override
                            public long extractTimestamp(FluentdMessage fluentdMessage, long l) {
                                return System.currentTimeMillis();
                            }
                        })
                )
//                .assignTimestampsAndWatermarks(new WatermarkStrategy<FluentdMessage>() {
//                    @Override
//                    public WatermarkGenerator<FluentdMessage> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
//                        return new WatermarkGenerator<FluentdMessage>() {
//                            private final long maxTimeLag = 5000; // 5 seconds
//
//                            @Override
//                            public void onEvent(FluentdMessage event, long eventTimestamp, WatermarkOutput output) {
//                                // don't need to do anything because we work on processing time
//                                output.emitWatermark(new Watermark(eventTimestamp));
//                            }
//
//                            @Override
//                            public void onPeriodicEmit(WatermarkOutput output) {
//                                //output.emitWatermark(new Watermark(System.currentTimeMillis()));
//                            }
//                        };
//                    }
//                })
                .name("Kafka Source");

        DataStream<Tuple2<String, Long>> keyedCounts  = stream
                .filter(value -> value.getGrokName() != null)
                .map(new MapFunction<FluentdMessage, Tuple2<String, Long>>() {
                    @Override
                    public Tuple2<String, Long> map(FluentdMessage value) throws Exception {
                        return Tuple2.of(value.getGrokName(), 1L);
                    }
                })
//                .assignTimestampsAndWatermarks(new WatermarkStrategy<Tuple2<String, Long>>() {
//                    @Override
//                    public WatermarkGenerator<Tuple2<String, Long>> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
//                        return new WatermarkGenerator<Tuple2<String, Long>>() {
//                            private final long maxTimeLag = 5000; // 5 seconds
//
//                            @Override
//                            public void onEvent(Tuple2<String, Long> event, long eventTimestamp, WatermarkOutput output) {
//                                // don't need to do anything because we work on processing time
//                            }
//
//                            @Override
//                            public void onPeriodicEmit(WatermarkOutput output) {
//                                output.emitWatermark(new Watermark(System.currentTimeMillis()));
//                            }
//                        };
//                    }
//                })
                .keyBy(value -> value.f0)
                .window(SlidingProcessingTimeWindows.of(Time.minutes(30), Time.seconds(30)))
                .trigger(PurgingTrigger.of(ProcessingTimeTrigger.create()))
                //.sum(2);
                .reduce((ReduceFunction<Tuple2<String, Long>>) (data1, data2) -> Tuple2.of(data1.f0, data1.f1 + data2.f1));

//        DataStream<Tuple2<String, Long>> generatedSource = env.addSource(UUIDSource.create(5));
//
//        DataStream<Tuple2<String, Long>> keyedCounts = generatedSource
//                .assignTimestampsAndWatermarks(new WatermarkStrategy<Tuple2<String, Long>>() {
//                    @Override
//                    public WatermarkGenerator<Tuple2<String, Long>> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
//                        return new WatermarkGenerator<Tuple2<String, Long>>() {
//                            private final long maxTimeLag = 1000; // 1 seconds
//
//                            @Override
//                            public void onEvent(Tuple2<String, Long> event, long eventTimestamp, WatermarkOutput output) {
//                                // don't need to do anything because we work on processing time
//                            }
//
//                            @Override
//                            public void onPeriodicEmit(WatermarkOutput output) {
//                                output.emitWatermark(new Watermark(System.currentTimeMillis()));
//                            }
//                        };
//                    }
//                })
//                .keyBy(value -> value.f0)
//                .window(SlidingProcessingTimeWindows.of(Time.minutes(5), Time.seconds(30)))
//                .trigger(PurgingTrigger.of(ProcessingTimeTrigger.create()))
//                //.window(TumblingProcessingTimeWindows.of(Time.seconds(30)))
//                //.timeWindow(Time.seconds(30))
//                //.sum(2);
//                .reduce((ReduceFunction<Tuple2<String, Long>>) (data1, data2) -> Tuple2.of(data1.f0, data1.f1 + data2.f1));


       keyedCounts
                //.windowAll(TumblingProcessingTimeWindows.of(Time.seconds(30)))
                .windowAll(SlidingProcessingTimeWindows.of(Time.minutes(30), Time.seconds(30)))
                .trigger(PurgingTrigger.of(ProcessingTimeTrigger.create()))
                .process(new ProcessAllWindowFunction<Tuple2<String, Long>, Tuple5<String, Long, Long, String, Long>, TimeWindow>() {

                    private ValueState<Long> currentCount;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        currentCount = getRuntimeContext().getState(
                                new ValueStateDescriptor<>("count", Long.class));
                    }

                    @Override
                    public void process(Context context,
                                        Iterable<Tuple2<String, Long>> iterable,
                                        Collector<Tuple5<String, Long, Long, String, Long>> out) throws Exception {
                        long count = StreamSupport.stream(iterable.spliterator(), false).count();
                        if(currentCount.value() == null) {
                            currentCount.update(0L);
                        }
                        Iterator<Tuple2<String, Long>> iterator = iterable.iterator();
                        Map<String, Long> map = new HashMap<>();
                        Map<String, List<Long>> keyTotalMap = new HashMap<>();

                        if(currentCount.value() < count) {
                            while (iterator.hasNext()) {
                                Tuple2<String, Long> tuple = iterator.next();
                                map.put(tuple.f0, keyDifference(tuple.f0, iterable));
                                keyTotalMap.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
                                //out.collect(Tuple3.of(tuple.f0, keyDifference(tuple.f0, iterable), sum(iterable)));
                            }

                            map.forEach((key, value) -> {
                                if(value > 0L) {
                                    out.collect(Tuple5.of(
                                            key,
                                            value,
                                            sum(key, keyTotalMap),
                                            getChiSqrLoggerScore(value, sumKeys(map), sum(key, keyTotalMap), sum(keyTotalMap)),
                                            System.currentTimeMillis()));
                                }});

                            //out.collect(Tuple5.of(null, null, null, null, null));
                            currentCount.update(count);
                        } else {
                            //This is currently the only way to force the job to end
                            throw new InterruptedException();
                        }
                    }
                })
               .addSink(new RichChiLoggerInputSink())
               .name("Postgres Sink");

        //globalCounts.writeAsText("s3://argo-workflow-bucket/output.txt");
        env.execute("Flink Kafka Chi Log Runner");
    }



//    private static class SummingWindowFunction implements WindowFunction<Tuple4<String, String, Long, Long>, Tuple5<String, String, Long, Long, Long>, String, Window> {
//        @Override
//        public void apply(String key, Window window, Iterable<Tuple4<String, String, Long, Long>> values, Collector<Tuple5<String, String, Long, Long, Long>> out) throws Exception {
//            long sum = 0L;
//            for (Tuple2<String, Long> value : values) {
//                sum += value.f1;
//            }
//
//            out.collect(new Tuple5<String, String, Long, Long, Long>(key, sum));
//        }
//
//    }

    private static class ProcessCounts extends ProcessAllWindowFunction<Long, Tuple4<String, String, Long, Long>, TimeWindow> {

        private ValueState<Tuple4<String, String, Long, Long>> state;

//        @Override
//        public void open(Configuration parameters) throws Exception {
//            state = getRuntimeContext().getState(
//                    new ValueStateDescriptor<Tuple4<String, String, Long, Long>>("state", typeInfo.getTypeClass()));
//        }

        @Override
        public void process(Context context,
                            Iterable<Long> iterable,
                            Collector<Tuple4<String, String, Long, Long>> out) throws Exception {
            Tuple4<String, String, Long, Long> current = state.value();
        }
    }

    private static class CountAccumulator implements AggregateFunction<Tuple4<String, String, Long, Long>, LongAdder, Long> {

        @Override
        public LongAdder createAccumulator() {
            return new LongAdder();
        }

        @Override
        public LongAdder add(Tuple4<String, String, Long, Long> value, LongAdder acc) {
            acc.add(value.f2);
            return acc;
        }

        @Override
        public Long getResult(LongAdder acc) {
            return acc.longValue();
        }

        @Override
        public LongAdder merge(LongAdder a, LongAdder b) {
            a.add(b.longValue());
            return a;
        }
    }

    private static long sumKeys(Map<String, Long> map) {
       return map.entrySet().stream().mapToLong(l -> l.getValue()).sum();
    }

    private static long sum(Iterable<Tuple2<String, Long>> values) {
        long sum = 0L;
        for (Tuple2<String, Long> value : values) {
            sum += value.f1;
        }
        return sum;
    }

    private static long sum(String key, Iterable<Tuple2<String, Long>> values) {
        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : values) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }
        return map.get(key).stream().mapToLong(l -> l).sum();
    }

    private static long sum(String key, Map<String, List<Long>> map) {

        List<Long> values = map.get(key);

        Collections.sort(values);

        int size = values.size();

        return values.get(size - 1);
    }

    private static long sum(Map<String, List<Long>> map) {
        return map.entrySet().stream().mapToLong(entry -> entry.getValue().get(entry.getValue().size() - 1)).sum();
    }

    private static long keyDifference(String key, Iterable<Tuple2<String, Long>> tuples) {
        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : tuples) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }

        List<Long> values = map.get(key);

        Collections.sort(values);

        int size = values.size();
        if(size >= 2) {
            return values.get(size - 1) - values.get(size - 2);
        } else {
            return values.get(size - 1);
        }
    }

    private static Map<String, Long> setInitialMapValues(Iterable<Tuple2<String, Long>> values) {
        Map<String, Long> map = new HashMap<>();
        values.forEach(value -> map.put(value.f0, value.f1));
        return map;
    }

    private static void sessionMap(Map<String, Long> map, String key, long count){
        map.put(key, count);
    }

    private static long getTotalCount(Map<String, Long> map) {
        long sum = 0L;
        for(Map.Entry<String, Long> entry : map.entrySet()) {
            sum += entry.getValue();
        }
        return sum;
    }


    private static FlinkKafkaConsumer<FluentdMessage> getConsumer(Properties props) {

        FlinkKafkaConsumer<FluentdMessage> consumer = new FlinkKafkaConsumer<>(
                "chi-logger-input",
                new InputMessageDeserializationSchema(), props);
        //consumer.assignTimestampsAndWatermarks(WatermarkStrategy.forBoundedOutOfOrderness(Duration.ofSeconds(20)));
//        Map<KafkaTopicPartition, Long> map = new HashMap<KafkaTopicPartition, Long>();
//        map.put(new KafkaTopicPartition("fluentd", 0), 20060L);
//        consumer.setStartFromSpecificOffsets(map);
        consumer.setStartFromEarliest();

        return consumer;
    }

    private static class LogTimestamp extends AscendingTimestampExtractor<Tuple3<String, Long, Long>> {
        private static final long serialVersionUID = 1L;

        @Override
        public long extractAscendingTimestamp(Tuple3<String, Long, Long> element) {
            return element.f2;
        }
    }

    /**
     * Calculates the cumulative probability of the word occurring in the document collection
     *
     * @param countNew   number of times word occurs in the document
     * @param totalNew total number of words in the document
     * @param countOld   number of times word occurs in document collection
     * @param totalOld total number of words in the document collection
     * @return the cumulative probability of the word occurring in the document collection
     */
    private static String getChiSqrLoggerScore(long countNew, long totalNew, long countOld, long totalOld) {

        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(5);
        double dblCountNew = new Long(countNew).doubleValue();
        double dblTotalNew = new Long(totalNew).doubleValue();
        double dblCountOld = new Long(countOld).doubleValue();
        double dblTotalOld = new Long(totalOld).doubleValue();

        ChiSquaredDistribution chi = new ChiSquaredDistribution(1d);
        double p = dblCountNew / dblTotalNew;
        double q = dblCountOld / dblTotalOld;
        if (p < q) return df.format(0d);
        double t = (dblCountNew + dblTotalOld) / (dblTotalNew + dblTotalOld);
        if (t == 0) {
            System.out.println("attempted division by zero.");
            return df.format(0d);
        }
        double v = dblCountNew * Math.log(p / t) + dblCountOld * Math.log(q / t);
        if (t == 1) return df.format(2d * v);
        if (p < 1) v += (dblTotalNew - dblCountNew) * Math.log((1 - p) / (1 - t));
        if (q < 1) v += (dblTotalOld - dblCountOld) * Math.log((1 - q) / (1 - t));
        v += v;

        return df.format(1d - chi.cumulativeProbability(v));
    }

    // *************************************************************************
    // USER FUNCTIONS
    // *************************************************************************

    private static class UUIDSource implements SourceFunction<Tuple2<String, Long>> {

        private static final long serialVersionUID = 1L;
        private Integer[] uuids;

        private UUID uuid;

        private volatile boolean isRunning = true;

        private UUIDSource(int number) {
            uuids = new Integer[number];
        }

        public static UUIDSource create(int number) {
            return new UUIDSource(number);
        }

        @Override
        public void run(SourceContext<Tuple2<String, Long>> ctx) throws Exception {

            while (isRunning) {
                Thread.sleep(5 * 1000);
                ctx.collectWithTimestamp(Tuple2.of("d56658a7-7566-40c6-822a-d3ac60feab1a", 1L), System.currentTimeMillis());
                ctx.collectWithTimestamp(Tuple2.of("606bb0a6-5a64-3eac-9635-6424dfccdc68", 1L), System.currentTimeMillis());
                ctx.collectWithTimestamp(Tuple2.of("ad7a6946-2b65-490d-a510-a6e2568f3c0e", 1L), System.currentTimeMillis());
                ctx.emitWatermark(new org.apache.flink.streaming.api.watermark.Watermark(System.currentTimeMillis()));
//                for (int i = 0; i < uuids.length; i++) {
//                    uuid = UUID.randomUUID();
//
//                    Tuple2<String, Long> record = new Tuple2<>(String.valueOf(uuid), 1L);
//                    ctx.collect(record);
//                    Thread.sleep(5 * 1000);
//                    ctx.collect(record);
//                }
            }
        }

        @Override
        public void cancel() {
            isRunning = false;
        }
    }


}
