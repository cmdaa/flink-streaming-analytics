    package io.cmdaa.streaming.java.jdbc;

import io.cmdaa.streaming.java.jdbc.function.RichLogInputSink2;
import io.cmdaa.streaming.java.kafka.function.GrokFilter;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import io.cmdaa.streaming.java.kafka.schema.InputMessageDeserializationSchema;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.internals.KafkaTopicPartition;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class LogConsumer2 {


    public static void main(String[] args) throws Exception {

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "kafka-cp-kafka-headless:9092");
//        properties.setProperty("group.id", "flink-group");
//        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "io.confluent.kafka.serializers.json.KafkaJsonSchemaDeserializer");
//        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializerr");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStream<FluentdMessage> stream = env
                .addSource(getConsumer(properties))
                .name("Kafka Source");

        stream
                .filter(new GrokFilter())
                .map(new MapFunction<FluentdMessage, Tuple4<String, String, Long, Long>>() {
                    @Override
                    public Tuple4<String, String, Long, Long> map(FluentdMessage value) throws Exception {
                        return Tuple4.of(value.getGrokName(), value.getHostname(), 1L, System.currentTimeMillis());
                    }
                })
                .keyBy(new KeySelector<Tuple4<String, String, Long, Long>, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> getKey(Tuple4<String, String, Long, Long> value) throws Exception {
                        return Tuple2.of(value.f0, value.f1);
                    }
                }).sum(2)
                .addSink(new RichLogInputSink2())
                .name("Postgres Sink");

        env.execute("Flink Kafka Log Consumer 2");

    }

    private static FlinkKafkaConsumer<FluentdMessage> getConsumer(Properties props) {

        FlinkKafkaConsumer<FluentdMessage> consumer = new FlinkKafkaConsumer<FluentdMessage>(
                "fluentd",
                new InputMessageDeserializationSchema(), props);
        Map<KafkaTopicPartition, Long> map = new HashMap<KafkaTopicPartition, Long>();
        map.put(new KafkaTopicPartition("fluentd", 0), 2006L);
        consumer.setStartFromSpecificOffsets(map);
        //consumer.setStartFromLatest();

        return consumer;
    }


}
