package io.cmdaa.streaming.java.jdbc.function;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.api.java.tuple.Tuple5;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;

public class RichLogInputSink extends RichSinkFunction<Tuple5<String, String, Integer, Long, Long>> {

    private static final String UPSERT_MESSAGE = "INSERT INTO public.flink_data (customer_id, month, expenses) "
            + "VALUES (?, ?, ?) ";

    private static final String UPSERT_FLUENTD_MESSAGE  =
            "INSERT INTO public.fluentd_table_1 (msg_id, host, hour_of_day, count, last_updated) "
                    + "VALUES (?, ?, ?, ?, ?) "
                    + "ON CONFLICT (msg_id, host, hour_of_day) "
                    + "DO UPDATE SET count = EXCLUDED.count, last_updated = EXCLUDED.last_updated";

    private PreparedStatement statement;

    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection connection =
                DriverManager.getConnection(
                        "jdbc:postgresql://postgres-postgresql.cmdaa.svc.cluster.local:5432/postgres?user=cmdaa&password=cmdaa");

        statement = connection.prepareStatement(UPSERT_FLUENTD_MESSAGE);
    }

    @Override
    public void invoke(Tuple5<String, String, Integer, Long, Long> message, Context context) throws Exception {

        statement.setString(1, message.f0);
        statement.setString(2, message.f1);
        statement.setInt(3, message.f2);
        statement.setLong(4, message.f3);
        statement.setLong(5, message.f4);
        statement.addBatch();
        statement.executeBatch();

    }
}
