//package io.cmdaa.streaming.java.kafka.consumer;
//
//
//import org.apache.flink.connector.jdbc.JdbcOutputFormat;
//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.streaming.api.datastream.DataStream;
//import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
//import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.ProducerRecord;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.common.serialization.StringSerializer;
//
//import io.cmdaa.streaming.java.kafka.model.InputMessage;
//import io.cmdaa.streaming.java.kafka.model.OutputMessage;
//import io.cmdaa.streaming.java.kafka.consumer.schema.InputMessageDeserializationSchema;
//import io.cmdaa.streaming.java.kafka.function.CapitalizeValue;
//import io.cmdaa.streaming.java.kafka.consumer.schema.LogOutputSchema;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.Properties;
//
//
//public class LogConsumer {
//
//    private static Logger logger = LoggerFactory.getLogger(LogConsumer.class);
//
//    public static void main(String[] args) throws Exception {
//        // set up the batch execution environment
//        logger.info("STARTING JOB");
//
//        Properties properties = new Properties();
//        properties.setProperty("bootstrap.servers", "kafka-cp-kafka-headless:9092");
//        properties.setProperty("group.id", "flink-group");
//
//        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//
////        DataStream<InputMessage> stream = env
////                .addSource(getConsumer(properties));
//
////        stream
////                .map(new CapitalizeValue())
////                .addSink(getProducer(properties));
//
//
//        // execute program
//        env.execute("Flink Kafka Log Consumer");
//    }
//
//
////    private static FlinkKafkaConsumer<InputMessage> getConsumer(Properties props) {
////
////        FlinkKafkaConsumer<InputMessage> consumer = new FlinkKafkaConsumer<InputMessage>(
////                "flink-topic",
////                new InputMessageDeserializationSchema(), props);
////        consumer.setStartFromEarliest();
////
////        return consumer;
////    }
//
//    private static FlinkKafkaProducer<OutputMessage> getProducer(Properties props) {
//
//        FlinkKafkaProducer<OutputMessage> producer = new FlinkKafkaProducer<OutputMessage>(
//                "flink-output-topic", new LogOutputSchema("flink-output-topic"),
//                props, FlinkKafkaProducer.Semantic.EXACTLY_ONCE);
//
//        return producer;
//    }
//
//    private static JdbcOutputFormat outputFormat() {
//
//        String query = "INSERT INTO public.flink_data (key, value) VALUES (?, ?)";
//        JdbcOutputFormat jdbcOutput = JdbcOutputFormat.buildJdbcOutputFormat()
//                .setDrivername("org.postgresql.Driver")
//                .setDBUrl("jdbc:postgresql://postgres-postgresql.cmdaa.svc.cluster.local:5432/postgres?user=cmdaa&password=cmdaa")
//                .setQuery(query)
//                .finish();
//
//        return jdbcOutput;
//
//    }
//
//
//    private static KafkaProducer<String, String> getKafkaProducer(InputMessage message) {
//
//        Properties properties = new Properties();
//        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka-cp-kafka-headless:9092");
//        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
//        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, null);
//
//        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
//
//        //ProducerRecord<String, String> record = new ProducerRecord<String, String>("flink-output-topic", message.getKey(), message.getValue());
//
//
//        return null;
//    }
//
//}
