package io.cmdaa.streaming.java.kafka.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.*;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.JsonNodeType;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.Header;

import org.apache.flink.api.java.tuple.Tuple8;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OutputMessageSerializationSchema
        implements KafkaSerializationSchema<Tuple8<String, String, String, Long, Long, Long, Long, String>> {

    private static final Logger LOG = LoggerFactory.getLogger(OutputMessageSerializationSchema.class);

    static ObjectMapper objectMapper;

    static {

        objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    private String topic;

    public OutputMessageSerializationSchema(String topic) {
        this.topic = topic;
    }

    @Override
    public ProducerRecord<byte[], byte[]> serialize(Tuple8<String, String, String, Long, Long, Long, Long, String> tuple, @Nullable Long aLong) {

        byte[] blob = null;
        byte[] msgId = null;

        ObjectNode scoreNode = objectMapper.createObjectNode();
        ObjectNode scoreLabelNode = objectMapper.createObjectNode();

//        scoreLabelNode.put("new_count", tuple.f3);
//        scoreLabelNode.put("new_total_count", tuple.f4);
//        scoreLabelNode.put("old_count", tuple.f5);
//        scoreLabelNode.put("old_total_count", tuple.f6);

        try {
            scoreLabelNode.put("grok_id", tuple.f0);
            scoreLabelNode.put("node", tuple.f1);
            scoreLabelNode.put("units", "score");

            scoreNode.put("name", "analytic_chilogger");
            scoreNode.put("value", Double.parseDouble(tuple.f7));
            scoreNode.put("timestamp", System.currentTimeMillis());
            scoreNode.set("labels", scoreLabelNode);

            blob = objectMapper.writeValueAsString(scoreNode).getBytes();
            msgId = objectMapper.writeValueAsString(tuple.f0).getBytes();
        } catch (JsonProcessingException e) {
            LOG.error("Method threw an error: {}", e.getStackTrace());
        } catch (NumberFormatException e) {
            LOG.error("Number Format Exception");
        }

        ProducerRecord<byte[], byte[]> record = new ProducerRecord<>(topic, msgId, blob);

        Headers headers = record.headers();
        headers.add("msg_id", msgId);
        headers.add("payload", blob);
        return record;
    }
}
