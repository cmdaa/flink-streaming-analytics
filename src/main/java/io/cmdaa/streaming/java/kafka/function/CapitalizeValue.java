package io.cmdaa.streaming.java.kafka.function;

import io.cmdaa.streaming.java.kafka.model.InputMessage;
import io.cmdaa.streaming.java.kafka.model.OutputMessage;
import org.apache.flink.api.common.functions.MapFunction;

public class CapitalizeValue implements MapFunction<InputMessage, OutputMessage> {

    @Override
    public OutputMessage map(InputMessage inputMessage) throws Exception {

        return new OutputMessage(inputMessage.getCustomerId(), inputMessage.getMonth(), inputMessage.getExpenses() * 2);
    }
}
