package io.cmdaa.streaming.java.kafka.schema;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.JsonStringEncoder;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import io.cmdaa.streaming.java.kafka.model.LogDataTable;

import java.io.IOException;
import org.apache.flink.table.shaded.org.joda.time.format.DateTimeFormatter;

import org.apache.flink.table.runtime.functions.DateTimeFunctions;

public class LogDataTableDeserializer extends StdDeserializer<LogDataTable> {

    public LogDataTableDeserializer() {
        this(null);
    }

    public LogDataTableDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LogDataTable deserialize(JsonParser parser, DeserializationContext ctx)
            throws IOException {
        //JsonStringEncoder encoder = JsonStringEncoder.getInstance();
        JsonNode tableNode = parser.getCodec().readTree(parser);
        LogDataTable table = new LogDataTable();
        table.setLogName(tableNode.get("log-data").get("log-name").textValue());
        //table.setLogId(tableNode.get("log-data").get("log-id").textValue());
        table.setInsertTime(tableNode.get("insert-time").longValue());
        //table.setRunId(tableNode.get("log-data").get("run-id").textValue());
        table.setS3Path(tableNode.get("log-data").get("s3-path").textValue());
        table.setS3Bucket(tableNode.get("log-data").get("s3-bucket").textValue());
        return table;
    }

}
