package io.cmdaa.streaming.java.kafka.function;

import io.cmdaa.streaming.java.kafka.model.InputMessage;
import org.apache.flink.api.common.functions.MapFunction;

import org.apache.flink.api.java.tuple.Tuple2;


public class RedisValue implements MapFunction<InputMessage, Tuple2<String,String>> {

    @Override
    public Tuple2<String, String> map(InputMessage inputMessage) throws Exception {
        return new Tuple2<String, String>(
                String.valueOf(inputMessage.getCustomerId()),
                String.valueOf(inputMessage.getExpenses())
        );
    }
}
