package io.cmdaa.streaming.java.kafka.function;

import io.cmdaa.streaming.java.kafka.model.CountWithTimestamp;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;


import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;
import org.apache.flink.api.common.state.ValueState;

import org.apache.flink.configuration.Configuration;

public class CountWithTimeoutFunction extends
        KeyedProcessFunction<Tuple3<String, String, Integer>, FluentdMessage, Tuple5<String, String, Integer, Long, Long>>{

    private ValueState<CountWithTimestamp> state;

    @Override
    public void open(Configuration parameters) throws Exception {
        state = getRuntimeContext().getState(
                new ValueStateDescriptor<CountWithTimestamp>("state", CountWithTimestamp.class));
    }

    @Override
    public void processElement(FluentdMessage value,
                               Context ctx,
                               Collector<Tuple5<String, String, Integer, Long, Long>> collector) throws Exception {


        CountWithTimestamp current = state.value();
        if(current == null){
            current = new CountWithTimestamp();
            current.grokName = value.getGrokName();
            current.hostName = value.getHostname();
            //current.hour_of_day = value.getHourOfDay();
        }

        current.count++;
        current.lastUpdated = ctx.timestamp();
        state.update(current);

        collector.collect(Tuple5.of(current.grokName, current.hostName, current.hour_of_day, current.count, current.lastUpdated));
    }

    @Override
    public void onTimer(long timestamp,
                        OnTimerContext ctx,
                        Collector<Tuple5<String, String, Integer, Long, Long>> out) throws Exception {
        CountWithTimestamp result = state.value();

        out.collect(Tuple5.of(result.grokName, result.hostName, result.hour_of_day, result.count, result.lastUpdated));

    }
}
