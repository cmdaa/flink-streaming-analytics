package io.cmdaa.streaming.java.kafka.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.TupleTypeInfo;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.flink.api.java.tuple.Tuple4;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public class InputMessageDeserializer implements KafkaDeserializationSchema<Tuple4<String, String, Long, Long>> {

    static ObjectMapper objectMapper;

    static {

        objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Override
    public boolean isEndOfStream(Tuple4<String, String, Long, Long> input) {
        return false;
    }

    @Override
    public Tuple4<String, String, Long, Long> deserialize(ConsumerRecord<byte[], byte[]> consumerRecord) throws Exception {
        FluentdMessage message = objectMapper.readValue(consumerRecord.value(), FluentdMessage.class);
        return Tuple4.of(message.getGrokName(), message.getHostname(), 1L, System.currentTimeMillis());
    }

    @Override
    public TypeInformation<Tuple4<String, String, Long, Long>> getProducedType() {
        return TupleTypeInfo.of(getProducedType().getTypeClass());
    }
}
