package io.cmdaa.streaming.java.kafka.model;

import org.apache.flink.api.java.tuple.Tuple3;

public class Key {

        private String grokName;
        private String hostname;
        private int hourOfDay;

        public Key(String grokName, String hostname, int hourOfDay) {
            this.grokName = grokName;
            this.hostname = hostname;
            this.hourOfDay = hourOfDay;
        }

        public String getGrokName() {
            return grokName;
        }

        public void setGrokName(String grokName) {
            this.grokName = grokName;
        }

        public String getHostname() {
            return hostname;
        }

        public void setHostname(String hostname) {
            this.hostname = hostname;
        }

        public int getHourOfDay() {
            return hourOfDay;
        }

        public void setHourOfDay(int hourOfDay) {
            this.hourOfDay = hourOfDay;
        }


}
