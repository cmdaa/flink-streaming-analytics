package io.cmdaa.streaming.java.kafka.function;


import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple;

public class KeyFieldExtractor<Type extends Tuple, Key> implements KeySelector<Type, Key>  {

    @Override
    public Key getKey(Type type) throws Exception {
        return type.getField(0);
    }
}
