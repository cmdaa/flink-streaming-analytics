package io.cmdaa.streaming.java.kafka.schema;

import com.fasterxml.jackson.databind.DeserializationFeature;
import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.apache.kafka.clients.consumer.ConsumerRecord;


public class InputMessageDeserializationSchema implements
        KafkaDeserializationSchema<FluentdMessage> {

    static ObjectMapper objectMapper;

    static {

        objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Override
    public boolean isEndOfStream(FluentdMessage inputMessage) {
        return false;
    }

    @Override
    public FluentdMessage deserialize(ConsumerRecord<byte[], byte[]> consumerRecord) throws Exception {

        return objectMapper.readValue(consumerRecord.value(), FluentdMessage.class);

        //message.setKey(objectMapper.readValue(consumerRecord.value(), String.class));
        //message.setValue(objectMapper.reader().forType(String.class).readValue(consumerRecord.value()));
        //return message;
    }

    @Override
    public TypeInformation<FluentdMessage> getProducedType() {
        return TypeInformation.of(FluentdMessage.class);
    }
}
