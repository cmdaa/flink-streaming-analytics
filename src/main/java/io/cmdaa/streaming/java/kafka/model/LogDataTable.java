package io.cmdaa.streaming.java.kafka.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.cmdaa.streaming.java.kafka.schema.LogDataTableDeserializer;

import java.util.Date;

@JsonSerialize
@JsonDeserialize(using = LogDataTableDeserializer.class)
public class LogDataTable {

    @JsonProperty("log-name")
    @JsonInclude(Include.NON_EMPTY)
    private String logName;

    @JsonProperty("log-id")
    @JsonInclude(Include.NON_EMPTY)
    private String logId;

    @JsonProperty("run-id")
    @JsonInclude(Include.NON_EMPTY)
    private String runId;

    @JsonProperty("system-file-path")
    @JsonInclude(Include.NON_EMPTY)
    private String systemFilePath;

    @JsonProperty("insert-time")
    @JsonInclude(Include.NON_EMPTY)
    private Long insertTime;

    @JsonProperty("sys-file")
    @JsonInclude(Include.NON_EMPTY)
    private String systemFile;

    @JsonProperty("node-labels")
    @JsonInclude(Include.NON_EMPTY)
    private String nodeLabels;

    @JsonProperty("daemonset-prefix")
    @JsonInclude(Include.NON_EMPTY)
    private String daemonsetPrefix;

    @JsonProperty("log-likeli")
    @JsonInclude(Include.NON_EMPTY)
    private double logLikeli;

    @JsonProperty("cos-sim")
    @JsonInclude(Include.NON_EMPTY)
    private double cosineSimilarity;

    @JsonProperty("s3-path")
    @JsonInclude(Include.NON_EMPTY)
    private String s3Path;

    @JsonProperty("s3-bucket")
    @JsonInclude(Include.NON_EMPTY)
    private String s3Bucket;

    public String getLogName() { return logName;}

    public void setLogName(String logName) {
        this.logName = logName;
    }

    public Long getInsertTime() { return insertTime;}

    public void setInsertTime(Long insertTime) { this.insertTime = insertTime; }

    public String getLogId() { return logId;}

    public String getRunId() { return runId;}

    public void setRunId(String runId) { this.runId = runId;}

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getSystemFilePath() {
        return systemFilePath;
    }

    public void setSystemFilePath(String systemFilePath) {
        this.systemFilePath = systemFilePath;
    }

    public String getSystemFile() {
        return systemFile;
    }

    public void setSystemFile(String systemFile) {
        this.systemFile = systemFile;
    }

    public String getNodeLabels() {
        return nodeLabels;
    }

    public void setNodeLabels(String nodeLabels) {
        this.nodeLabels = nodeLabels;
    }

    public String getDaemonsetPrefix() {
        return daemonsetPrefix;
    }

    public void setDaemonsetPrefix(String daemonsetPrefix) {
        this.daemonsetPrefix = daemonsetPrefix;
    }

    public double getLogLikeli() {
        return logLikeli;
    }

    public void setLogLikeli(double logLikeli) {
        this.logLikeli = logLikeli;
    }

    public double getCosineSimilarity() {
        return cosineSimilarity;
    }

    public void setCosineSimilarity(double cosineSimilarity) {
        this.cosineSimilarity = cosineSimilarity;
    }

    public String getS3Path() {
        return s3Path;
    }

    public void setS3Path(String s3Path) {
        this.s3Path = s3Path;
    }

    public String getS3Bucket() {
        return s3Bucket;
    }

    public void setS3Bucket(String s3Bucket) {
        this.s3Bucket = s3Bucket;
    }
}
