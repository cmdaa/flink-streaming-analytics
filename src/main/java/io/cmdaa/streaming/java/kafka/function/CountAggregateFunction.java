package io.cmdaa.streaming.java.kafka.function;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.api.common.state.ValueState;

public class CountAggregateFunction extends ProcessFunction<Tuple4<String, String, Long, Long>, Tuple5<String, String, Long, Long, Long>> {

    private transient ValueState<Tuple5<String, String, Long, Long, Long>> sum;

    @Override
    public void open(Configuration parameters) throws Exception {
        sum = getRuntimeContext().getState(
                new ValueStateDescriptor<Tuple5<String, String, Long, Long, Long>>("state",
                        TypeInformation.of(new TypeHint<Tuple5<String, String, Long, Long, Long>>() {
                })));
    }

    @Override
    public void processElement(Tuple4<String, String, Long, Long> value,
                               Context context,
                               Collector<Tuple5<String, String, Long, Long, Long>> out) throws Exception {
        Tuple5<String, String, Long, Long, Long> current = sum.value();

        current.f4 += value.f2;

        sum.update(current);

        out.collect(Tuple5.of(current.f0,current.f1,current.f2,current.f3,current.f4));

    }
}
