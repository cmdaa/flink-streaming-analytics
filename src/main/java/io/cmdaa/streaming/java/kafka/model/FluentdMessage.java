package io.cmdaa.streaming.java.kafka.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.base.Objects;

@JsonSerialize
public class FluentdMessage {

    @JsonProperty("message")
    @JsonInclude(Include.NON_EMPTY)
    private String message;

    @JsonIgnore
    private String timeOne;

    @JsonIgnore
    private String loglevelOne;

    @JsonIgnore
    private String javaOne;

    @JsonIgnore
    private String javaTwo;

    @JsonIgnore
    private String javaThree;

    @JsonIgnore
    private String numberOne;

    @JsonIgnore
    private String numberTwo;

    @JsonIgnore
    private String numberThree;

    @JsonProperty("sysfile")
    @JsonInclude(Include.NON_EMPTY)
    private String sysfile;

    @JsonProperty("logname")
    @JsonInclude(Include.NON_EMPTY)
    private String logId;

    @JsonProperty("grok_name")
    @JsonInclude(Include.NON_EMPTY)
    private String grokName;

    @JsonProperty("grokfailure")
    private String grokFailure;

    @JsonProperty("hostname")
    private String hostname;

    @JsonProperty("count")
    private double count;

    @JsonIgnore
    private String hostOne;

    @JsonIgnore
    private String hostTwo;

    @JsonIgnore
    private String hostThree;

    @JsonIgnore
    private String hostFour;

    @JsonIgnore
    private String hostFive;

    @JsonIgnore
    private String pid;

    @JsonIgnore
    private String ppid;


    public FluentdMessage(){
    }

    public String getSysfile() {return sysfile; }

    public void setSysfile(String sysfile) { this.sysfile = sysfile; }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGrokName() {
        return grokName;
    }

    public void setGrokName(String grokName) {
        this.grokName = grokName;
    }

    public String getLogId() { return logId; }

    public void setLogId(String logId) { this.logId = logId; }

    public String getGrokFailure() {
        return grokFailure;
    }

    public void setGrokFailure(String grokFailure) {
        this.grokFailure = grokFailure;
    }

    public double getCount() { return count; }

    public void setCount(double count) { this.count = count; }

    public String getHostname() {
        return hostname;
    }

    public String getTimeOne() {
        return timeOne;
    }

    public String getLoglevelOne() {
        return loglevelOne;
    }

    public String getJavaOne() {
        return javaOne;
    }

    public String getNumberOne() {
        return numberOne;
    }

    public String getNumberTwo() {
        return numberTwo;
    }

    public String getJavaTwo() {
        return javaTwo;
    }

    public void setJavaTwo(String javaTwo) {
        this.javaTwo = javaTwo;
    }

    public String getNumberThree() {
        return numberThree;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setTimeOne(String timeOne) {
        this.timeOne = timeOne;
    }

    public void setLoglevelOne(String loglevelOne) {
        this.loglevelOne = loglevelOne;
    }

    public void setJavaOne(String javaOne) {
        this.javaOne = javaOne;
    }

    public void setNumberOne(String numberOne) {
        this.numberOne = numberOne;
    }

    public void setNumberTwo(String numberTwo) {
        this.numberTwo = numberTwo;
    }

    public void setNumberThree(String numberThree) {
        this.numberThree = numberThree;
    }

    public String getJavaThree() {
        return javaThree;
    }

    public void setJavaThree(String javaThree) {
        this.javaThree = javaThree;
    }

    public String getHostOne() {
        return hostOne;
    }

    public void setHostOne(String hostOne) {
        this.hostOne = hostOne;
    }

    public String getHostTwo() {
        return hostTwo;
    }

    public void setHostTwo(String hostTwo) {
        this.hostTwo = hostTwo;
    }

    public String getHostThree() {
        return hostThree;
    }

    public void setHostThree(String hostThree) {
        this.hostThree = hostThree;
    }

    public String getHostFour() {
        return hostFour;
    }

    public void setHostFour(String hostFour) {
        this.hostFour = hostFour;
    }

    public String getHostFive() {
        return hostFive;
    }

    public void setHostFive(String hostFive) {
        this.hostFive = hostFive;
    }

    public String getPpid() {
        return ppid;
    }

    public void setPpid(String ppid) {
        this.ppid = ppid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FluentdMessage message1 = (FluentdMessage) o;
        return Objects.equal(grokName, message1.grokName) &&
                Objects.equal(hostname, message1.hostname);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(grokName, hostname);
    }
}