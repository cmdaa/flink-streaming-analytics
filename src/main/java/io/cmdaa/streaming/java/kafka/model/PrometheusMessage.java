package io.cmdaa.streaming.java.kafka.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PrometheusMessage {


    @JsonProperty("log_id")
    private String msgId;

    @JsonProperty("hostname")
    private String hostname;

    @JsonProperty("score")
    private String score;

    @JsonProperty("count_new")
    private int countNew;

    @JsonProperty("total_new")
    private int totalNew;

    @JsonProperty("count_old")
    private int countOld;

    @JsonProperty("total_old")
    private long totalOld;

    public String getMsgId() {
        return msgId;
    }

    public String getHostname() {
        return hostname;
    }

    public String getScore() {
        return score;
    }

    public int getCountNew() {
        return countNew;
    }

    public int getTotalNew() {
        return totalNew;
    }

    public int getCountOld() {
        return countOld;
    }

    public long getTotalOld() {
        return totalOld;
    }
}
