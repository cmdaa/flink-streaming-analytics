package io.cmdaa.streaming.java.kafka.function;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;

public class GrokKeySelector implements  KeySelector<FluentdMessage, Tuple2<String, String>>{

    @Override
    public Tuple2<String, String> getKey(FluentdMessage fluentdMessage) throws Exception {
        if(fluentdMessage.getGrokName() != null) {
            return Tuple2.of(fluentdMessage.getGrokName(), fluentdMessage.getHostname());
        }
        return Tuple2.of("missing_grok", "hostname");
    }

}
