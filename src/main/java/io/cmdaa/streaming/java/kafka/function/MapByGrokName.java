package io.cmdaa.streaming.java.kafka.function;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple4;

import java.time.LocalDateTime;

public class MapByGrokName implements MapFunction<FluentdMessage, Tuple4<String, String, Integer, Integer>> {

    @Override
    public Tuple4<String, String, Integer, Integer> map(FluentdMessage fluentdMessage) throws Exception {
        if(fluentdMessage.getGrokName() != null){
            return Tuple4.of(fluentdMessage.getGrokName(), fluentdMessage.getHostname(), LocalDateTime.now().getHour(), 1);
        }
        return Tuple4.of("","",0,0);

    }


}
