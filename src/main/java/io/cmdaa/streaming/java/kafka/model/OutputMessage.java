package io.cmdaa.streaming.java.kafka.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OutputMessage {

    @JsonProperty("customer_id")
    private long customerId;

    @JsonProperty("month")
    private int month;

    @JsonProperty("expenses")
    private double expenses;

    public OutputMessage(long customerId, int month, double expenses) {
        this.customerId = customerId;
        this.month = month;
        this.expenses = expenses;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setExpenses(double expenses) {
        this.expenses = expenses;
    }

    public long getCustomerId() {
        return customerId;
    }

    public int getMonth() {
        return month;
    }

    public double getExpenses() {
        return expenses;
    }
}
