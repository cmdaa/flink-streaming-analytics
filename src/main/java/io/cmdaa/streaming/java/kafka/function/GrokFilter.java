package io.cmdaa.streaming.java.kafka.function;

import io.cmdaa.streaming.java.kafka.model.FluentdMessage;
import org.apache.flink.api.common.functions.FilterFunction;

public class GrokFilter implements FilterFunction<FluentdMessage> {

    @Override
    public boolean filter(FluentdMessage fluentdMessage) throws Exception {
        return fluentdMessage.getGrokName() != null;
    }
}
