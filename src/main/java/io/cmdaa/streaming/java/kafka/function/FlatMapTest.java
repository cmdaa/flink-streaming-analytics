package io.cmdaa.streaming.java.kafka.function;

import io.cmdaa.streaming.java.kafka.model.InputMessage;

import org.apache.flink.api.common.functions.ReduceFunction;

public class FlatMapTest implements ReduceFunction<InputMessage> {

    @Override
    public InputMessage reduce(InputMessage val1, InputMessage val2) throws Exception {
        InputMessage message = new InputMessage();
        message.setMonth(val1.getMonth() + val2.getMonth());
        message.setExpenses(val1.getCustomerId() + val2.getCustomerId());
        return message;
    }
}
