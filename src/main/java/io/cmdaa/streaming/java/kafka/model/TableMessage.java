package io.cmdaa.streaming.java.kafka.model;

public class TableMessage {

    private String msg_id;
    private String host;
    private int hour_of_day;
    private long cnt;
    private long last_updated;

    public TableMessage() {

    }


    public TableMessage(String msg_id, String host, int hour_of_day, long cnt, long last_updated) {
        this.msg_id = msg_id;
        this.host = host;
        this.hour_of_day = hour_of_day;
        this.cnt = cnt;
        this.last_updated = last_updated;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getHour_of_day() {
        return hour_of_day;
    }

    public void setHour_of_day(int hour_of_day) {
        this.hour_of_day = hour_of_day;
    }

    public long getCnt() {
        return cnt;
    }

    public void setCnt(long cnt) {
        this.cnt = cnt;
    }

    public long getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(long last_updated) {
        this.last_updated = last_updated;
    }

    @Override
    public String toString() {
        return "GrokByHourCounts{" +
                "msgId = " + msg_id +
                ", host = " + host +
                ", hourOfDay = " + hour_of_day +
                ", count = " + cnt +
                ", lastUpdated = " + last_updated +
                "}";
    }
}
