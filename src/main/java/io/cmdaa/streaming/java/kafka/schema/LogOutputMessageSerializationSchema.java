package io.cmdaa.streaming.java.kafka.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.flink.api.java.tuple.Tuple4;

import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;

public class LogOutputMessageSerializationSchema implements
        KafkaSerializationSchema<Tuple4<String, String, String, Long>> {

    private static final Logger LOG = LoggerFactory.getLogger(LogOutputMessageSerializationSchema.class);

    static ObjectMapper objectMapper;

    static {

        objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    private String topic;

    public LogOutputMessageSerializationSchema(String topic) {
        this.topic = topic;
    }

    @Override
    public ProducerRecord<byte[], byte[]> serialize(Tuple4<String, String, String, Long> tuple, @Nullable Long aLong) {

        byte[] blob = null;
        byte[] msgId = null;

        ObjectNode node = objectMapper.createObjectNode();
        ObjectNode labelNode = objectMapper.createObjectNode();

        labelNode.put("grok_id", tuple.f0);
        labelNode.put("node", tuple.f1);
        labelNode.put("log_id", tuple.f2);
        labelNode.put("units", "count");

        node.put("name", "analytic_grok_match");
        node.put("value", tuple.f3);
        node.put("timestamp", System.currentTimeMillis());
        node.set("labels", labelNode);

        try {
            blob = objectMapper.writeValueAsString(node).getBytes();
            msgId = objectMapper.writeValueAsString(tuple.f0).getBytes();
        } catch (JsonProcessingException e) {
            LOG.error("Method threw an error: {}", e.getStackTrace());
        }

        ProducerRecord<byte[], byte[]> record = new ProducerRecord<>(topic, msgId, blob);

        Headers headers = record.headers();
        headers.add("msg_id", msgId);
        headers.add("payload", blob);
        return record;
    }




}
