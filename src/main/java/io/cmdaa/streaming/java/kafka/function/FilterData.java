package io.cmdaa.streaming.java.kafka.function;

import org.apache.flink.api.common.functions.FilterFunction;

import io.cmdaa.streaming.java.kafka.model.InputMessage;

public class FilterData implements FilterFunction<InputMessage> {

    @Override
    public boolean filter(InputMessage inputMessage) throws Exception {
        if(inputMessage.getMonth() == 12) {
            return true;
        }

        return false;
    }
}
