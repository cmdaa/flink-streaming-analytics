package io.cmdaa.streaming.java.kafka.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;


@JsonSerialize
public class InputMessage {

    @JsonProperty("cust_id")
    long customerId;
    @JsonProperty("month")
    int month;
    @JsonProperty("expenses")
    double expenses;

    public InputMessage() {
    }

    public InputMessage(long customerId, int month, double expenses) {
        this.customerId = customerId;
        this.month = month;
        this.expenses = expenses;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setExpenses(double expenses) {
        this.expenses = expenses;
    }

    public long getCustomerId() {
        return customerId;
    }

    public int getMonth() {
        return month;
    }

    public double getExpenses() {
        return expenses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InputMessage message1 = (InputMessage) o;
        return Objects.equal(customerId, message1.customerId) &&
                Objects.equal(month, message1.month) &&
                Objects.equal(expenses, message1.expenses);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(customerId, month, expenses);
    }
}