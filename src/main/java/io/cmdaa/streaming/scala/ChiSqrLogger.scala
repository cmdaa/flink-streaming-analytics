package io.cmdaa.streaming.scala

import org.apache.commons.math3.distribution.ChiSquaredDistribution

object ChiSqrLogger {

  /**
   * Calculates the cumulative probability of the word occurring in the document collection
   *
   * @param dococc   number of times word occurs in the document
   * @param doctotal total number of words in the document
   * @param allocc   number of times word occurs in document collection
   * @param alltotal total number of words in the document collection
   * @return the cumulative probability of the word occurring in the document collection
   */
  def computeLogLikelihood(dococc: Int, doctotal: Int, allocc: Int, alltotal: Int): Double = {
    val docOccDouble = dococc.toDouble
    val docTotDouble = doctotal.toDouble
    val allOccDouble = allocc.toDouble
    val allTotDouble = alltotal.toDouble
    val chi: ChiSquaredDistribution = new ChiSquaredDistribution(1d)
    val p = docOccDouble / docTotDouble
    val q = allOccDouble / allTotDouble
    if (p < q) return 0
    val t = (docOccDouble + allOccDouble) / (docTotDouble + allTotDouble)
    if (t == 0) {
      println("attempted division by zero.")
      return 0d
    }
    var v = docOccDouble * Math.log(p / t) + allOccDouble * Math.log(q / t)
    if (t == 1) return 2d * v
    if (p < 1) v += (docTotDouble - docOccDouble) * Math.log((1 - p) / (1 - t))
    if (q < 1) v += (allTotDouble - allOccDouble) * Math.log((1 - q) / (1 - t))
    v += v
    return 1d - chi.cumulativeProbability(v)
  }



}
