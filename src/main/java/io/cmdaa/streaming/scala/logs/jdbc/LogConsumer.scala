package io.cmdaa.streaming.scala.logs.jdbc


import java.util. Properties
import java.time._

import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer, FlinkKafkaProducer}
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema
import org.apache.kafka.clients.consumer.ConsumerRecord

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.{DefaultScalaModule}
import com.fasterxml.jackson.annotation.JsonProperty

object LogConsumer {

  case class LogMessage(@JsonProperty("grok_name") msg_id: String, @JsonProperty("hostname") host: String)

  def main(args: Array[String]) {

    val now = ZonedDateTime.now(ZoneId.of("America/New_York"))

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val properties = new Properties();
    properties.setProperty("bootstrap.servers", "kafka-cp-kafka-headless:9092")

    val kafkaConsumer = new FlinkKafkaConsumer[LogMessage](
      "fluentd",
      KafkaInputSchema,
      properties
    )

    kafkaConsumer.setStartFromEarliest()

    val json = env.addSource(kafkaConsumer).name("Kafka Source")

    json
        .filter(_.msg_id != null)
        .map(value => (value.msg_id, value.host, now.getHour(), 1L, System.currentTimeMillis()))
        .keyBy(value => (value._1, value._2, value._3))
        .sum(3)
        .addSink(new PostgresSink)
        .name("Postgres Sink")

    env.execute("Log Consumer")

  }

  object KafkaInputSchema extends KafkaDeserializationSchema[LogMessage] with Serializable {

    import org.apache.flink.api.common.typeinfo.TypeInformation
    import org.apache.flink.api.java.typeutils.TypeExtractor

    @transient lazy val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

    override def isEndOfStream(t: LogMessage): Boolean = false

    override def deserialize(consumerRecord: ConsumerRecord[Array[Byte], Array[Byte]]): LogMessage = mapper.readValue(consumerRecord.value(), classOf[LogMessage])

    override def getProducedType: TypeInformation[LogMessage] = TypeExtractor.getForClass(classOf[LogMessage])
  }
}
