package io.cmdaa.streaming.scala.logs.jdbc

import java.sql.{Connection, DriverManager, PreparedStatement, SQLException}

import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
//import org.slf4j.{Logger, LoggerFactory}

class PostgresSink extends RichSinkFunction[(String, String, Int, Long, Long)] {

  //val logger: Logger = LoggerFactory.getLogger("Postgres Sink")
  var conn: Connection = _
  var ps: PreparedStatement = _
  val jdbcUrl = "jdbc:postgresql://postgres-postgresql.cmdaa.svc.cluster.local:5432/postgres?user=cmdaa&password=cmdaa"
  val driverName = "org.postgresql.Driver"
  val preparedStatement = "INSERT INTO public.fluentd_table_1 (msg_id, host, hour_of_day, count, last_updated) " +
                          "VALUES (?, ?, ?, ?, ?) " +
                          "ON CONFLICT (msg_id, host, hour_of_day) " +
                          "DO UPDATE SET count = EXCLUDED.count, last_updated = EXCLUDED.last_updated"

  override def open(parameters: Configuration): Unit = {

    Class.forName(driverName)
    try {
      Class.forName(driverName)
      conn = DriverManager.getConnection(jdbcUrl)

      // close auto commit
      conn.setAutoCommit(false)
    } catch {
      case e@(_: ClassNotFoundException | _: SQLException) =>
        //logger.error("init mysql error")
        e.printStackTrace()
        System.exit(-1);
    }
  }


  override def invoke(message: (String, String, Int, Long, Long)): Unit = {

    ps = conn.prepareStatement(preparedStatement)
    ps.setString(1, message._1)
    ps.setString(2, message._2)
    ps.setInt(3, message._3)
    ps.setLong(4, message._4)
    ps.setLong(5, message._5)

    ps.execute()
    conn.commit()
  }


  override def close(): Unit = {
    if (conn != null) {
      conn.commit()
      conn.close()
    }
  }
}
