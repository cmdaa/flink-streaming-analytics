package io.cmdaa.test.java;


import org.apache.flink.api.java.tuple.Tuple2;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class WordCapitalizeIntegrationText {

    public static ObjectMapper mapper;

    public static List<Tuple2<String, Long>> tuples = new ArrayList<>();


    @Before
    public void setup() {

        mapper = new ObjectMapper().registerModule(new JavaTimeModule());

        tuples.add(Tuple2.of("d56658a7-7566-40c6-822a-d3ac60feab1a",6L));
        tuples.add(Tuple2.of("ad7a6946-2b65-490d-a510-a6e2568f3c0e",6L));
        tuples.add(Tuple2.of("606bb0a6-5a64-3eac-9635-6424dfccdc68",6L));
        tuples.add(Tuple2.of("d56658a7-7566-40c6-822a-d3ac60feab1a",12L));
        tuples.add(Tuple2.of("ad7a6946-2b65-490d-a510-a6e2568f3c0e",12L));
        tuples.add(Tuple2.of("606bb0a6-5a64-3eac-9635-6424dfccdc68",12L));
        tuples.add(Tuple2.of("d56658a7-7566-40c6-822a-d3ac60feab1a",18L));
        tuples.add(Tuple2.of("ad7a6946-2b65-490d-a510-a6e2568f3c0e",18L));
        tuples.add(Tuple2.of("606bb0a6-5a64-3eac-9635-6424dfccdc68",18L));


    }

    @Ignore
    @Test
    public void givenDataSet_whenExecuteWordCapitalizer_thenReturnCapitalizedWords() throws Exception {

        //InputMessage message = new InputMessage("1", "Value01");
        byte[] messageSerialized = mapper.writeValueAsBytes("value01");
        String value = mapper.readValue(messageSerialized, String.class);

        assertEquals(value, "value01");

    }

    @Ignore
    @Test
    public void check_zoned_time() throws Exception {
        int hour = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("America/New_York")).getHour();
        System.out.println("Hour of day is: " + hour);
    }

    @Ignore
    @Test
    public void flattenIterableValues() throws Exception {

//        Optional<Tuple2<String, Long>> values = tuples.stream()
//                .reduce((val1, val2) -> Tuple2.of(val2.f0, val1.f1 - val2.f1));

        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : tuples) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }

       Map<Object, Object> finalMap = map.entrySet().stream()
                .collect(
                        Collectors.toMap(
                                key -> key.getKey(),
                                e -> e.getValue().stream().reduce((a, b) -> b - a)
                        )
                );

        finalMap.entrySet().stream().forEach(values -> System.out.println("Key = " + values.getKey() + "  Value = " + values.getValue()));

//        tuples.stream()
//                .map(value -> value.f0)
//                .
//                .reduce(new BinaryOperator<Long>() {
//                    @Override
//                    public Long apply(Long val1, Long val2) {
//                        return val1 - val2;
//                    }
//                });
    }
    @Test
    public void testList() throws Exception {
        Map<String, List<Long>> map = new HashMap<>();
        for(Tuple2<String, Long> tuple : tuples) {
            map.computeIfAbsent(tuple.f0, k -> new ArrayList<>()).add(tuple.f1);
        }

        List<Long> values = map.get("606bb0a6-5a64-3eac-9635-6424dfccdc68");

        Collections.sort(values);

        int size = values.size();
        if(size >= 2) {
            System.out.println(values.get(size - 1) - values.get(size - 2));
        } else {
            System.out.println(values.get(size - 1));
        }
    }

    @Test
    public void testDecimal() throws Exception {
        DecimalFormat df = new DecimalFormat("#.#####");
        df.setMaximumFractionDigits(5);
        System.out.println("The decimal format looks likke" + df.format(0.0));
    }


}
