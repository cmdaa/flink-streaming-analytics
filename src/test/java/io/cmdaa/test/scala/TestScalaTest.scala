package io.cmdaa.test.scala

import io.cmdaa.test.java.TestJava
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}

@RunWith(classOf[JUnitRunner])
class TestScalaTest extends FlatSpec{

//  before {
//    // do something before each unit test
//    print("before")
//  }
//
//  after {
//    // do something after each unit test
//    print("after")
//  }

  "print scala" should "instantiate the scala object and print the message" in {

    val testScala = new TestScala("Hello Scala")
    testScala.print()
  }

  "print java" should "instantiate the scala object and print the message" in {

    val testJava = new TestJava()
    testJava.print("Hello Java")
  }
}
