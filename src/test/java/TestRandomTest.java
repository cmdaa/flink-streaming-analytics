import org.junit.Test;

import java.text.DecimalFormat;

public class TestRandomTest {

    @Test
    public void testDecimal() throws Exception {
        DecimalFormat df = new DecimalFormat("#.#####");
        df.setMaximumFractionDigits(5);
        System.out.println("The decimal format looks like " + Double.parseDouble(df.format(0d)));
    }

}
