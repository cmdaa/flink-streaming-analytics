FROM flink

MAINTAINER Robert Cullen

RUN mkdir -p $FLINK_HOME/usrlib

RUN mkdir -p /opt/flink/plugins/flink-s3-fs-hadoop
RUN ln -fs /opt/flink/opt/flink-s3-fs-hadoop-*.jar /opt/flink/plugins/flink-s3-fs-hadoop/.

ARG JAR_FILE
COPY target/${JAR_FILE} $FLINK_HOME/usrlib/cmdaa-flink-job.jar