# Flink Streaming Analytics

Flink is used to analyze log messages and metrics from various sources in CMDAA.  Flink can be run as a batch or streaming analytic. A Flink job can be written in one of these languages: Java, Scala or Python.

##  Build

To run a batch or streaming analytic in Flink you compile your job from source into a .jar file.  The .jar file is then uploaded via the Flink dashboard.

```bash
mvn clean package
```

## Execute Job

From the Flink dashboard click the "Submit New Job" link.  Upload the .jar file.  If not already populated by the maven build, select the file and enter the Class name (full path) containing the main method for your Flink job. Click "Submit".

## Building for CMDAA

Currently CMDAA runs three jobs in Flink to do analytics.  These class names correspond to these tags:
```editorconfig
cmdaa-log-counts -> io.cmdaa.streaming.java.jdbc.LogCounts
cmdaa-chi-logger -> io.cmdaa.streaming.java.jdbc.ChiLogger
cmdaa-grokfailures -> io.cmdaa.streaming.java.jdbc.GrokFailures
```


In the pom.xml edit the transformer section of the maven-release plugin. Choose which main class to compile and release.

``` xml
<transformers>
    <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
        <mainClass>io.cmdaa.streaming.java.jdbc.ChiLogger
        </mainClass>
    </transformer>
</transformers>
```
From the command line run the maven command:
``` bash
mvn clean verify
```
Then run the docker build command with tag:
``` bash
docker build --build-arg JAR_FILE=streaming-1.0-SNAPSHOT.jar -t cmdaa/flink:cmdaa-chi-logger-v1 .
```
Then push the docker image to dockerhub:
``` bash
docker push cmdaa/flink:cmdaa-chi-logger-v1
```