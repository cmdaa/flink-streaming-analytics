FROM flink

WORKDIR $FLINK_HOME

ARG CLUSTER_ID=flink-jobmanager
ARG JOB_IMAGE=flink:cmdaa-chi-logger-v1
ARG KAFKA_SERVER
ARG KAFKA_TOPIC

ENTRYPOINT ./bin/flink run-application
    --target kubernetes-application \
    -Dkubernetes.cluster-id=$CLUSTER_ID \
    -Dkubernetes.container.image=cmdaa/$JOB_IMAGE \
    local:///opt/flink/usrlib/cmdaa-flink-job.jar --kafka-server $KAFKA_SERVER --kafka-topic $KAFKA_TOPIC